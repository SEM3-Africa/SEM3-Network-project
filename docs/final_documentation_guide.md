# Final Documentation Formatting  Document

This document will aim to cover each point set out in the requirements document supplied by the Project owner (Mentors) and as such i will try to go into detail on each matter to make sure we have hit each point.

    Document Creation
    **************************
    Author(s)   : Kyle Mcshane
    Date        : 01-10-2018
    Audience    : Team Members

## Revision History

| Date       | Version | Change                                            | Changed by   | Email               |
| ---------- | ------- | ------------------------------------------------- | ------------ | ------------------- |
| 01-10-2018 | 1.0     | Creation of document                              | Kyle Mcshane | kyle0055@edu.eal.dk |
| 01-10-2018 | 2.0     | Added a lot more sections and renamed the document | Kyle Mcshane | kyle0055@edu.eal.dk |

## Layout of Final Documentation

This section will relate to how the final documentation of the hand in should look and as such we should aim to hit each of the topics with the following information in the following order.

| Section | Topic                        | Description                                                                                                              |
| ------- | ---------------------------- | ------------------------------------------------------------------------------------------------------------------------ |
| 1       | Cover Page                   | Page with team names - name of project - name of mentors                                                                 |
| 2       | Table of Contents            | A Detailed list of pages contained in the document should not contain the cover pages and the ToC & ToF itself.          |
| 3       | Table of Figures             | A list of all pictures and figures used in the document.                                                                 |
| 4       | Points of Contact            | Should be set out with each members contact details.                                                                     |
| 5       | Revision History             | A track of the running changes of the document by who / when and the relevant version numbers                            |
| 6       | Introduction                 | This will encompass the initial project description and a lot of the information                                         |
| 7       | Service Needs                | Requirements set out by the customer that will need to be covered in order to reach a successful outcome of the project. |
| 8       | Automation                   | Documentation relating to methods used in order to achieve a level of automation within the project.                     |
| 9       | Routing Policy Requirements  | Routing policy information for each device on the final network                                                          |
| 10      | Project Constraints          | Problems we encountered during the project and the steps taken to overcome them or reach a compromise.                   |
| 11      | Project Purpose & Scope      | The reasoning behind we are creating the project and any future plans and such intended for the project                  |
| 12      | Technical Challenges         | Technical issues encountered within the project relating to software and devices                                         |
| 13      | Functional Requirements      | To which standard we should reach in order to reach the minimal functional requirement of the project.                   |
| 14      | Technical Requirements       | Hardware and software requirements in order to replicate the network                                                     |
| 15      | Firewall Policy Requirements | Firewall information on the final network and the reasoning why                                                          |

## S1 - Cover Page

___

This page shall be the professional presentation of the project and as such should aim to look professional and deliver the relevant information of what the document is about, as such it should contain the following attributes..

1. Title of Project with Group Number
2. Sub-title with small description
3. Logo Of Institution (UCL)
4. Group Members + Email Addresses
5. Mentors Name + Contact Address
6. Date of submission.

## S2 - Table of Contents

___

A relatively simple page that should contain every page and corresponding page number within the document - however this page should not cover the Cover page, ToC & ToF in the page numbering, and as such the introduction should be denoted as page 1.

## S3 - Table of Figures

___

This section of the document should contain every figure contained within the document with the corresponding text - this is to give a detailed list and reference point to figures in the project.

## S4 & S5 - Points of Contact & Revision History

___

In aim of keeping the documents information slim and not too over bearing the following two sections should be merged into a single page if the document is not too detailed.

### S4 - Points of Contact

Points of contact shall aim to contain every member of the project and their full contact details and role within the group and as such should be laid out in the following format.

| Contact Name | Contact Type | Site/Continent | Telephone Number | Email               | Oversight Function |
| ------------ | ------------ | -------------- | ---------------- | ------------------- | ------------------ |
| Kyle Mcshane | Consultant   | [5] Africa     | #                | kyle0055@edu.eal.dk | Roles              |

### S5 - Revision History

As stated in the document provided by the mentor we should format our revision history in a format with the following points

1. Date     - Should be in the following format DD-MM-YYYY
2. Version  - See section below (should be a in the final format NN.NN)
3. Change   - Description of the changes included
4. Page     - Which pages the changes occurred on (Major)
5. Email    - The email address of the one who made the changes

### Version
  
In addition to the version history we should format the version numbers in the following format

* Major changes and formatting should be denoted by the version increasing by one whole number - it is important to track this as a significant overall impact-full change to the document - such as version 1.0 going to version 2.0
* Minor changes should be denoted in the revision history by an increase in the decimal number - changes that will encompass this will be minor formatting changes, small new sections, and spelling fixes.

It is important to note however minor a change should always be covered by a revision history and if a minor change needs to be implemented more should be aimed to be implemented with it as to avoid needless spam of versions of the document.

## S6 - Introduction

___

This section of the document should include all information relating to the initial description of the document such as what the project aims to achieve and what level of connectivity it will provide.

As a further explanation of this this section should be the customer-facing side of the project where we in detail explain the ins-and-outs of our decisions and why we use certain protocols and equipment - a lot of the information required for this section should be already generated in the HLD document we made for the project.

## S7 - Service Needs

___

Requirements set out on equipment and software are as followed.

|---|---|---|---|

## S8 - Automation

___

This section will include all relevant information relating to our use of Ansible for configurations in the project, the way we used them and how they were useful.
it is important in this section to start on a basic level and increase the level of knowledge more towards the end, in addition we should be an argument and case for each place we automize things.

## S9 - Routing Policy Requirements

___

Information from IS-IS to BGP and the documentation relating to it - how we connect our inner AS to the MPLS back-bone.

## S10 - Project Constraints

___

This section will hold details relating to the constraints we had in the project - things that held us back and the hurdles / compromises we had to endure to fulfil the project as a whole - additionally we should include the lessons learned document in this section as it will relate heavily.

## S11 - Project Purpose & Scope

___

Needs more explanation in this section as im currently unsure of its contents - KM 01-10-18

## S12 - Technical Challenges

___

This section will hold documentation relating to the physical limitations of hardware and the logical issues relating to software used in the project and the compromises we made.

## S13 - Functional Requirements

___

The documentation in this section will rely heavily on the HLD but will only display the minimal requirements set out by the project owner.
this could include stuff like testing plans and methods of monitoring successful configurations.

## S14 - Technical Requirements

___

Hardware and software relating to the project - exactly what you would need to replicate the network and justify their usage within the network.

## S15 - Firewall Policy Requirements

___

Mainly configurations for the SRX firewall will be stored here and the reasoning for the filtering on each policy, additionally if we plan to set up firewalls on each of the hosts in the system then we should note them here too along with their purpose and reasoning for their firewall functionality.