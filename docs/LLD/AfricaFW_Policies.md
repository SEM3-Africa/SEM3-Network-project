# Documentation and requirements of the firewall policies.

* DHCP Requests must only be allowed from within the site.
* Telnet is only allowed within the sites and must be blocked from other teams sites.
* Documentation of what policies allow what users certain services & networks.
* COS must be implemented on the MPLS backbone and if desired the site too.

| Interface | From Zone	| To Zone 	| protocols | Action | Source Address   | Source Mask   | Source Port   | Destination Address   | Destination Mask	| Destination Port  | Applications  | Notes 		|
| --------- | --------- | --------- | --------- | ------ | ---------------- | ------------- | ------------- | --------------------- | ----------------- | ----------------- | ------------- | ------------- |
| ge-0/0/1	| Untrust	| Trust		| Telnet	| Reject | any			 	| 0				| 0				| any					| 0					| 0					|				|				|
| ge-0/0/1	| Untrust	| Trust		| Telnet	| Reject | any			 	| 0				| 0				| any					| 0					| 0					|				|				|
| ge-0/0/1	| Untrust	| Trust		| Telnet	| Reject | any			 	| 0				| 0				| any					| 0					| 0					|				|				|
| ge-0/0/2	| Untrust	| Trust		| DHCP		| Deny	 | any			 	| 0				| 0				| 2001:0:5: 			| 0					| 0					|				|				|
| ge-0/0/2	| Untrust	| Trust		| DHCP		| Deny	 | any			 	| 0				| 0				| 2001:0:5:		    	| 0					| 0					|				|				|
| ge-0/0/2	| Untrust	| Trust		| DHCP		| Deny	 | any			 	| 0				| 0				| 2001:0:5:			    | 0					| 0					|				|				|
| ge-0/0/2	| Untrust	| Trust		| HTTP		| Allow	 | any			 	| 0				| 0				| 2001:0:5:<Webserver>	| 0					| 80				|				| DMZ Webserver	|

# Policy limitations
- All sites have access to the Webserver in the DMZ zone
- Allow ping to webserver and hosts