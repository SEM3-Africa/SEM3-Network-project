# LibreNMS Documentation

	Author	: Tim Nikolajsen
	Date	: 20/11/18
	Purpose	: Describe LibreNMS and how it is used in the Project.
	Audience: Project Members

	
## Revision

| Revision  | Date       | Comments                                      								| Changes By  	 |
| --------- | ---------- | ----------------------------------------------------------------------------	| -------------- |
| 1.0       | 20/11/2018 | Initial Document                              								| Tim Nikolajsen |



# Description

LibreNMS is a monitoring system which gets its network information from the SNMP protocol from each device on the network.
It displays the SNMP information in the form of graphs, logs, lists, and maps.

# How it is used in the project

LibreNMS is used to check the if devices go down as well as keeping track of each device's performance history.