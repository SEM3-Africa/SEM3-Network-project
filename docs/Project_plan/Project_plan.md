# Project Plan for Group 5 - Africa

	Author	: Tim Nikolajsen
	Date	: 26/9/18
	Purpose	: Describe the end product as well as the steps to get there.
	Audience: Project Members

	
## Revision

| Revision  | Date       | Comments                                      								| Changes By  	 |
| --------- | ---------- | ----------------------------------------------------------------------------	| -------------- |
| 1.0       | 24/09/2018 | Initial Document                              								| Tim Nikolajsen |
| 1.1		| 25/09/2018 | Removed stakeholders and changed goals/sub-goals to product and milestones 	| Tim Nikolajsen |


## Background

This project was requested by 'The Company' to enable multiple departments to communicate with other departments on multiple continents around the world.

Other groups are creating similar networks on other continents with different specifications.

Other continents: Australia, Russia, Asia, USA, and Europe


## Purpose

The purpose of this project is to implement a network for "The Companys" departments in Africa (Angola, South Africa, and Nigeria).

To allow each department to communicate with each other, South Africa will be connected to an MPLS backbone to enable communication between all departments.


## Goals

### Product

Implementation of a site that fits the requirement of the Sevencontinents projectplan. That has connectivity through the MPLS backbone. Along with a fully documented project.

### Milestones

* Have an internal network fully working with:
	- Connectivity between the user LANs
	- Setup Automation Server (Ansible Tower or Flask)
* Create a Docker environment with:
	- Monitoring with Nagios, LibreNMS, Prometheus and Elastic search with MariaDB to store the data.
	- VPN
* Have an MPLS connection to the backbone made in collaboration with the project groups.
* User Authentication for ESXi


## Schedule
| **Start Date**| **End Date** 	|
| :-----------: | :-----------: |
| 20/08-2018	| 14/12-2018	|


## Organization

| **Project Manager**	|
| :-------------------- |
| Bo Mikkelsen	    	| 

| **Project Members** 	|
| :--------------------	|
| Kasper Jessen	    	|
| Kyle McShane	    	|
| Martin Nielsen    	|
| Tim Nikolajsen	    |

| **Customer**		    |
| :--------------------	|
| Peter Thomsen	    	|


## Budget and Resources
	
| **Personel required** | **Amount** 	|
| :-------------------- | :-----------:	|
| Network Technicians	| 5				|


## Risk Management

| **Risks** 														| **Response**																																	|
| :----------------------------------------------------------------	| :-------------------------------------------------------------------------------------------------------------------------------------------- |
| If the production blade breaks down and becomes unusable			| Take one of the reserve blades.																												|
| Other continents interfrastructor problems, so we can't connect.	| Respond by either helping, if we have the time and resources to help fix the issue. If we can't or must not, then we will just have to wait. 	|



## Communication

| **Stakeholder**	 		| **Communication Method**			| **Reason**  								|
| :------------------------ | :-------------------------------- | :---------------------------------------- |
| "The Company"	 			| Weekly presentations				| To present the owner with status updates.	|
| Consultant's company		| Weekly presentations				| To present employer with status updates.	|
| Other project groups		| Weekly meeting					| Discuss how to design the MPLS Network. 	|


## Perspectives

This project will affect "The Company" in a positive way as it creates a bigger area from where people can work from.


## Evaluation

If each user LAN on each continent can communicate with each other then the project is a success.

To achieve this each workstation must be able to ping all other workstations on the network.


## References

Project Requirements Document:
- https://gitlab.com/SEM3-Africa/SEM3-Network-project/blob/master/docs/Project_documentation/Five_Continents_Requirements_Document_v2.5.pdf (Requires login)

Project Management:
- https://gitlab.com/SEM3-Africa/SEM3-Network-project