* **Main Goals**: (Purpose of project) 
  * Implementation of a site that fits the requirment of the Sevencontinents projectplan. That has connectivity through the MPLS backbone. Along with full documented project
* **Sub goals**: 
  * Making a fully automated backup system (vMotion)
  * Full functional monitoring system including(nagios, librenms, prometheus and elastic search)
  * Authentication services (radius)
  * Creating a docker enviroment
  * Central automation server (Ansible-tower or Flask)