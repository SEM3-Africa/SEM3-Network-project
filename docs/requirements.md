# Requirements Document

    Author              : Kyle Mcshane
    Purpose             : To layout the final requirements for the hand-in
    Date of Creation    : 03-10-2018

## Revision History

| Revision | Date       | Comments                                                                      | Changes By   |
| -------- | ---------- | ----------------------------------------------------------------------------- | ------------ |
| 1.0      | 03-10-2018 | Document created in the aim for cross referencing for the final documentation | Kyle Mcshane |
| 2.0      | 08-10-2018 | Further revision to the document new sections and cross referencing           | Kyle Mcshane |
| 2.1      | 09-10-2018 | Minor Updates and initial project finalisation                                | Kyle Mcshane |
| 2.2      | 09-10-2018 | Formatting to the document headers                                            | Kyle Mcshane |
| 3.0      | 05-11-2018 | Group review of document and check marking                                    | Kyle Mcshane |

## [P13] - Functional Requirements

Basic functional requirements documented in page [13] of the project brief.
[Project Brief Link](https://gitlab.com/SEM3-Africa/SEM3-Network-project/blob/master/docs/Project_documentation/Five_Continents_Requirements_Document_v2.5.pdf)

| Requirement                     | Explanation                                                 | Progress |
| ------------------------------- | ----------------------------------------------------------- | -------- |
| vSwitches                       | Achieved in ESXI                                            | Complete |
| Routers                         | Achieved via MX / SRX                                       | Complete |
| Firewalls                       | Achieved via SRX                                            | Complete |
| Continent / Site Infrastructure | Route-able AS with VLANS [Minimum 2 sites]                  | Complete |
| MPLS Backbone Infrastructure    | Connectivity to other groups through PE and MPLS SRXS       | Complete |
| IP Addressing                   | IPv4 / IPv6 Connectivity                                    | Complete |
| ISIS Internal IGP               | Inter-Router adjacency and connectivity via the use of ISIS | Complete |
| Set AS Number                   | Assigned to team 5 as 65005                                 | Complete |

## [P14-15] - Technical Requirements

Requirements set out for documentation in pages [14-15] of the project brief.

| Requirement                                      | Explanation                                                                                               | Progress             |
| ------------------------------------------------ | --------------------------------------------------------------------------------------------------------- | -------------------- |
| Site High-Level-Document                         | Site & Continent information based on the configuration and layout of the AS                              | In Progress          |
| MPLS High-Level-Document                         | Information pertaining to the MPLS backbone and our Hub/PE router to it.                                  | Completed 05-11-2018 |
| Site IP Addressing Scheme (LLD)                  | Spreadsheet/Table with all devices and IPs in use with protocol information for the site                  | Completed 10-11-2018 |
| MPLS Addressing Scheme (LLD)                     | Spreadsheet/Table with all devices and IPs in use with protocol information for the MPLS backbone         | Completed 05-11-2018 |
| Configuration & Implementation of ESXI           | Information regarding the use of the ESXI and configuration                                               | Completed 05-11-2018 |
| Configuration & Implementation of vSphere        | Information regarding the installation and uses of vSphere                                                | Completed 28-11-2018 |
| Configuration of MX series router                | current release version must be specified                                                                 | Completed 05-11-2018 |
| Configuration and Implementation of SRX firewall | release version must be specified                                                                         | Completed 05-11-2018 |
| Configuration and Implementation of SRX 240 PE   | Information regarding the physical router that will be configured for connectivity with the MPLS backbone | Completed 05-11-2018 |
| Automation                                       | Ansible configurations / play-books - use of the ansible tower for config deployments                     | Completed 05-11-2018 |
| Back-up plan / Configuration                     | Information regarding the use of software or services to back up the current deployment of the network    | Completed 28-11-2018 |

Some other points that are provided by this section of the document are the following...

1. The site must be hosted on the ESXI blade.
2. ESXI must be managed with vSphere - [Statement] - Need to make a case for not using this on linux + it is out of date.
3. vSwitches must be partitioned with "port teams/groups"
4. VMX Connectivity [ Two Options ]
    1. vMX connected via port-teams (one per location)
    2. vMX are connected directly and one is connected to the vSwitch
5. vSRX is connected to the vSwitch
6. IPv6 over IPv4 Tunnels in the MPLS backbone done using GRE or 6t04 tunnels using IBGP connectivity from CE to PE (6t04)
7. No Single point of failure must exist ensuring the network adheres to a 99.99% uptime as telco-ready.  (Needs Reviewied / Dicussion with peter)

## [P14] - IP Addressing

### IPv4

IPv4 use in the project.

| Requirement                                       | Notes                                                                                                                                                | Progress             |
| ------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------- |
| Must conform to CIDR Addressing                   | [Link can be found to it here](https://www.digitalocean.com/community/tutorials/understanding-ip-addresses-subnets-and-cidr-notation-for-networking) | Completed 05-11-2018 |
| Use Class A & C addresses only                    | Public addresses are usable where necessary only                                                                                                     | Completed 05-11-2018 |
| DHCP must be used for all nodes and LAN equipment | There must be only one DHCP Server                                                                                                                   | Completed 05-11-2018 |

### IPv6

IPv6 use in the project.

| Requirement                                 | Notes                       | Progress             |
| ------------------------------------------- | --------------------------- | -------------------- |
| Unique Local Addresses                      | In Site infrastructure only | Completed 05-11-2018 |
| EUI-64 or /127 Site Local Addresses for PtP | Point-to-point links        | Completed 05-11-2018 |

Additional information on IPv6...

* EUI64 can be used for locally attached equipment (Workstations & Servers) instead of DHCP
* In the case DHCPv6 is used it is only allowed on one server.

### MPLS Backbone

 Completed 05-11-2018

IP use within the MPLS infrastructure

* Must run IPv4
* IPv6 Traffic must be using 6PE or another tunneling protocol over IPv4. (A Inter-team consensus must be reached on this)

### Policy Requirements

Regarding both firewalls and routing ...

## [P16] - Routing Policy Requirements

    To do

This section should cover the documentation of routes and how they are being redistribute throughout the sites.

## [P16] - Firewall Policies

    Completed 05-11-2018

Documentation and requirements of the firewall policies.

* DHCP Requests must only be allowed from within the site.
* Telnet is only allowed within the sites and must be blocked from other teams sites.
* Documentation of what policies allow what users certain services & networks.
* COS must be implemented on the MPLS backbone and if desired the site too.

## [P17] - Site Design (Technical Implementation)

Location of devices and services

An important note is that all of the following should have documentation relating to them.

| Requirement                         | Notes                                                                                                      | Progress             |
| ----------------------------------- | ---------------------------------------------------------------------------------------------------------- | -------------------- |
| DHCP Server connected to main site. | If Required                                                                                                | Completed 05-11-2018 |
| DNS Server (if desired)             | Must be documented                                                                                         | Completed 05-11-2018 |
| Web-server on a DMZ                 | Connected between the SRX-FW and the MPLS-FW                                                               | Completed 01-11-2018 |
| FILE/DB server                      | Connected to 2 sub-sites (Workstation is optional)                                                         | Completed 07-09-2018 |
| ESXI NIC 1 Connectivity (PE/CE)     | Communication between the CE/PE MPLS routers must occur on NIC1                                            | Completed 05-11-2018 |
| ESXI NIC 2 VPN Connectivity         | VPN Connectivity should be located on NIC 2 for management network separation (We use a jumphost instead ) | Completed 05-11-2018 |
| vMX LAN Separation                  | must be separated from the LAN switches                                                                    | Completed 05-11-2018 |

## [P19] - Security

This section will pertain to the security requirements of the AS.

### System Security

Documentation of System security within the project.

| Requirements                   | Notes                                          | Progress             |
| ------------------------------ | ---------------------------------------------- | -------------------- |
| User Authentication            | via the use of keys or passwords               | Completed 05-11-2018 |
| Firewalls                      | to allow/disallow traffic on the network       | Completed 05-11-2018 |
| Public Access to Network       | DMZ / VLAN (DMZ)                               | Completed 05-11-2018 |
| Physical Access to the System  | precautions of accessing the companies data.   | To do                |
| Disaster prevention & recovery | Methods taken against the loss of service/data | Completed 05-11-2018 |
| System monitoring              | Monitoring system traffic for debugging        | Completed 25-10-2018 |

### Software Security

Documentation in regards to software used in the project.

| Requirement                  | Notes                          | Progress             |
| ---------------------------- | ------------------------------ | -------------------- |
| User Authentication          | Keys/Passwords                 | Completed 29-08-2018 |
| Electronic Data Transmission | Encryption                     | Completed 29-08-2018 |
| Electronic Data Storage      | Methods of storing information | Completed 09-09-2018 |

### Hard-copy Security

Must be GDPR Compliant and cover any data generated in a physical format for the company.

| Requirement      | Notes | Progress   |
| ---------------- | ----- | ---------- |
| Hard-Copy Policy |       | Not needed |
| GDPR Compliancy  |       | Not needed |

## [P21] - MPLS Requirements

This section will cover the MPLS connectivity of the network including the backbone area that was configured by all teams involved in the project.

A table has been supplied for an overall requirement of the MPLS backbone this includes the following

| Feature                              | Requirement Type | Progress             |
| ------------------------------------ | ---------------- | -------------------- |
| Label Distribution Protocol (LDP)    | Not Wanted       | Do Not Implement     |
| Resource Reservation Protocol (RSVP) | Mandatory        | Completed            |
| eGBP PE to CE Router                 | Mandatory        | Completed 05-11-2018 |
| iBGP                                 | Mandatory        | Completed 05-11-2018 |
| IS-IS                                | Mandatory        | Completed 05-11-2018 |
| Traffic Engineering (TE)             | Mandatory        | Completed 05-11-2018 |
| Explicit Route Objects (ERO)         | Desired          | Completed 05-11-2018 |
| Class of Service                     | Desired          | Not implemented      |
| RSVP Link protection                 | Desired          | Completed 05-11-2018 |
| RSVP Node Protection                 | Desired          | Completed 05-11-2018 |
| Fast Reroute                         | Desired          | Not implemented      |
| Backup LSP                           | Desired          | Completed 05-11-2018 |

## [P28/9] - Automation

Automation must be a part of the business case and as such a case must be made and presented for it.

### [P29] - Operations Automation

    Use JUNOS OS Scripts to automate operation tasks & network troubleshooting, these should be implemented to hit a minimal requirement.
    We use ansible for reboot scripts, config saves & version control, push/merge/replace configs, juniper version to json so we will consider this closed [Netconf]. Completed 05-11-2018

The scripts should be able too...

* Create Custom Operational Mode commands.
* Execute a series of operational mode commands.
* Customize the output of operational commands.
* Shorten trouble shooting time by gathering operational information and narrowing down the cause of the network problem.
* Perform controlled configuration changes.
* Monitor the status of the device and send network warning parameters like high cpu usage.

### [P29] - Event Policies

    Completed 05-11-2018

This part of the automation is optional.

if-then-else policies that trigger when something happens such as...

* Ignoring The Event
* File Upload to a specified Destination
* Execute JUNOS Commands
* Execute JUNOS event scripts
* modify the configuration

Additionally JUNOS OS Event scripts should be used and defined by these event policies to take immediate action.

* Python Scripts.
* Ansible.
* Pyez.
* SQL or SQL lite.

## [P30] - Documentation Requirements

The following documents need to be  delivered to finalize the project and receive a grade.

| Document Type            | Notes                                                                                   | Progress             |
| ------------------------ | --------------------------------------------------------------------------------------- | -------------------- |
| Business Case            |                                                                                         | Review & Update      |
| Project Plan             |                                                                                         | Ongoing              |
| High Level Design        | Of both MPLS BB & Internal AS                                                           | Review & Update      |
| Low Level Design         | Of both MPLS BB & Internal AS                                                           | Completed 10-11-2018 |
| Naming Conventions       | Of both Nodes & Devices                                                                 | Completed 05-11-2018 |
| Weekly Progress Reports  | Power-point Presentations delivered weekly.                                             | Ongoing              |
| End of Stage Report      |                                                                                         | To do                |
| Business Continuity Plan | Plans and procedures on how to keep the business going in the event of project setbacks | Review & Update      |
| Final Project Report     | High level and very detailed report on the overall project and what it encompasses      | To do                |

### [P30] - Business Case

As part of the final documentation the business document must be up to standard and as such must follow the following.

* Cover the background of the project
* Expected benefits of the business for taking on this project.
* Options considered within the business case.  
* Expected Costs of the Project
* Gap Analysis and Expected Risks of undertaking the project.

These points should be included in the Business case document and a great deal of consideration should be put into each point, and as such a following format for the document has been provided.

1. Preface (Landing Page/Cover Sheet)
2. Table of Contents.
3. Executive Briefing.
    1. Recommendation - The purpose for the project
    2. Summary of Results - The expected outcome and benefits for the company if the project is undertaken
    3. Decision to be taken - Which direction the project will move
4. Introduction
    1. Business Drivers
    2. Scope
    3. Financial Metrics
5. Analysis
    1. Assumptions
    2. Costs
    3. Benefits
    4. Risk
    5. Strategic Options
    6. Opportunity Costs
6. Conclusions, Recommendation & Next Steps (Going Forward)

### [P32] - Business Continuity Plan

The business continuity plan should include the following information to meet the successful criteria.

| Document Type                                      | Notes                                                                                                      | Progress |
| -------------------------------------------------- | ---------------------------------------------------------------------------------------------------------- | -------- |
| Contact Information                                | Important contact information held at the start of the document also encompasses "initial data"            |          |
| Revision Management                                | A current running list of changes that have happened to the document and their changes detailed            |          |
| Purpose and Scope                                  | The purpose of the project and the scale of it and what it encompasses                                     |          |
| Plan Usage Guidelines                              | Procedures on how to use information within the document and when they will be triggered.                  |          |
| Policy Information                                 | Detailed steps of each of the policies/procedures                                                          |          |
| Emergency Response & Management                    | Much like a risk management system that will cover the steps taken in the event of a catastrophic failure. |          |
| Step-by-Step Procedures                            | Ties in closely with the Policy information                                                                |          |
| Check Lists and Flow Diagrams                      | Self Explanatory                                                                                           |          |
| Schedule for reviewing,testing & updating the plan | This is so the document will never become dated and irrelevant to the scale of the project.                |          |

It is important to note that the document gives examples such as the following ...

* If the site loses internet connectivity and people cannot use their business laptops/desktops/servers.
* What single points of failure exist and what risk management systems are in place.
* What are the critical outsourced dependencies and relationships (such as having services hosted by a third party.)
* During a disruption in service what are the methods in place to circumvent this and prevent further discord.
* The minimum amount of staff required and the functions that are required for them to operate.
* Key skills, Knowledge & Expertise needed to recover the business..
* Critical system and security controls that are in place in case the system goes down.

It also goes on to mention that the BCP planning process should take several steps those are ...

1. initiating the project.
2. Information gathering phase - business impact analysis (BIA) and risk analysis (RA).
3. Plan Development.
4. Plan Testing, Maintenance and updating.

### [P34] - HLD (High-Level-Design)

An example has been set out for how the HLD should be formatted, as such the following points have been provided

* Description of infrastructure (Continent  & MPLS Backbone)
* Pros/cons of the proposed infrastructure
* Hardware & Software used and required in the project
* Assignment of IP Networks to Different Networks (In CIDR Format)

### [P34] - LLD (Low-Level-Design)

This document should contain all technical and addresses relating to the project and as such should aim to include the following points...

* Detailed Description of the continent and site infrastructure.
* Spreadsheet that covers the following aspects.

| Point               | Note                                              | Completion           |
| ------------------- | ------------------------------------------------- | -------------------- |
| Host-IP Addresses   |                                                   | Completed 05-11-2018 |
| Cabling Information |                                                   | Completed 27-11-2018 |
| Routing Protocols   | information relating to its configuration.        | Completed 05-11-2018 |
| Routing Policies    | information relating to its configuration.        | Completed 10-11-2018 |
| Firewall Filters    | information relating to its configuration         | Completed 11-11-2018 |
| Security Parameters | Anything that needs to be specified in the system | Completed 9-11-2018  |

## [P35] - Document Closing

A table of RFCs is provided on what is supported in the project in which we can check off what is included in the project.