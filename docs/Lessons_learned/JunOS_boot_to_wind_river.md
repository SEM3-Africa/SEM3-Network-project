# vSRX adapter disappearance

	Author	: Tim Nikolajsen
	Date	: 02/11/18
	Purpose	: Describe cause and solution to JunOS devices booting to Wind River on ESXi.
	Audience: Project Members

	
## Revision

| Revision  | Date       | Comments                                      								| Changes By  	 |
| --------- | ---------- | ----------------------------------------------------------------------------	| -------------- |
| 1.0       | 02/11/18 	 | Initial Document                              								| Tim Nikolajsen |

## Problem

When configuring the network adapters for JunOS devices in an ESXi environment boots into "Wind River" and doesn't work.

## Solution

- All the adapters on the JunOS device needs to be type "VMXNET3"

## Notes

When it a JunOS device boots into Wind River it is because there is some sort of self test which looks at adaptor types.
JunOS devices are compatible with with "VMXNET3" and if the devices doesn't see these it won't boot into JunOS.