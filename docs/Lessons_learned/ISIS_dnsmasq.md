# ISIS DNSMASQ

	Author	: Tim Nikolajsen
	Date	: 03/10/18
	Purpose	: Describe the problem with DNSMASQ over ISIS on vMX
	Audience: Project Members

	
## Revision

| Revision  | Date       | Comments                                      								| Changes By  	 |
| --------- | ---------- | ----------------------------------------------------------------------------	| -------------- |
| 1.0       | 03/10/2018 | Initial Document                              								| Tim Nikolajsen |

## Problem

When trying to run DNSMASQ as DHCP server the vMX can see requests but not receive offers. (seen through monitor traffic)

## Solution

Use another routing protocol.

## Notes
