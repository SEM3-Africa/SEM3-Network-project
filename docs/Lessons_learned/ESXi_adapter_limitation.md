# ESXi adapter limitation and ordering.

	Author	: Tim Nikolajsen
	Date	: 19/9/18
	Purpose	: Describe the problem with limitation (and ordering) of adapters on the in WMware ESXi as well as a solution.
	Audience: Project Members

	
## Revision

| Revision  | Date       | Comments                                      								| Changes By  	 |
| --------- | ---------- | ----------------------------------------------------------------------------	| -------------- |
| 1.0       | 19/09/2018 | Initial Document                              								| Tim Nikolajsen |
| 2.0		| 26/09/2018 | Another problem was added (Adapter ordering)									| TIm Nikolajsen |

## Problem - Max number of adapters

When trying to setup vMX's as per our design (more than 10 NIC adapters) we noticed the adapter arrangement didn't match up with what the ESXi GUI showed.

### Solution

Use less than 10 interfaces or other Type-1 Hypervisor.

### Notes

ESXi has a limit of 10 adapters per virtual device. This includes both 


## Problem - Adapter ordering

When trying to setup vMX's as per our design we noticed the adapter arrangement didn't match up with what the ESXi GUI showed after about 5 adapters.

### Solution

- Either use less than 5 adapter
- Know how VMware rearranges the adapters.

### Notes

The way VMware is mapping the PCI slot numbers is what is creating this issue. The ordering can be changed by following matt dinham's guide located in the references section.

### References

"INTERFACE ORDERING ON VMWARE (VMX / VSRX / VQFX)"
http://matt.dinham.net/

"Mapping PCI slot numbers to guest-visible PCI bus topology "
https://kb.vmware.com/s/article/2047927