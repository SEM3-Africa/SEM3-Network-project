# vSRX adapter disappearance

	Author	: Tim Nikolajsen
	Date	: 26/9/18
	Purpose	: Describe the problem with dissapearance of adapters on the vSRX
	Audience: Project Members

	
## Revision

| Revision  | Date       | Comments                                      								| Changes By  	 |
| --------- | ---------- | ----------------------------------------------------------------------------	| -------------- |
| 1.0       | 18/09/2018 | Initial Document                              								| Tim Nikolajsen |

## Problem

When deploying a vSRX through the ESXi web client the interfaces will not show up in the "show interfaces terse" after being set up.

## Solution

- All the adapters on the vSRX needs to be type "VMXNET3"

## Notes

Adapter 0 is set as the management

It seems to be a compatibility problem with vSRX where is cannot communicate with other adapter types.

- You should only change the adapter type when the adapter is disconnected.