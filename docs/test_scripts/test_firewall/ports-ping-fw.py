import nmap, os, time, subprocess

test_mgmtsrx = 1
quit = 2

def main():
	choice = 0
    	while choice != quit:
        	# display the menu.
        	display_menu()
		choice = int(input('Enter your choice: '))
		print('-'*50)
		if choice == test_mgmtsrx:
			ping_mgmtsrx()
			port_mgmtsrx()	
		elif choice == quit:
			print('Exiting program!')
		else:
			print('Invalid selection!')
			print('-'*50)
			time.sleep(2)

def display_menu():
	print('1) Test firewall')
	print('2) Quit')

def ping_mgmtsrx():
	with open(os.devnull, "wb") as limbo:
		for n in xrange(1):
		        ip="2001:0:5:70::1".format(n)
		        result=subprocess.Popen(["ping", "-c", "1", "-n", "-W", "2", ip],
		                stdout=limbo, stderr=limbo).wait()
		        if result:
		                print ip, "inactive"
				print('Ping failed :-(')
		        else:
		                print ip, "active"
				print('Firewall ping successful! :-)')
				print('Scanning ports now. Wait a minute.')

def port_mgmtsrx():
	nm = nmap.PortScanner()
	nm.scan('-6 2001:0:5:70::1')
	nm.command_line()
	nm.scaninfo()
	nm.all_hosts()

	for host in nm.all_hosts():
	    print('-'*50)
	    print('Host : %s (%s)' % (host, nm[host].hostname()))
	    print('State : %s' % nm[host].state())
	    for proto in nm[host].all_protocols():
		print('-'*50)
		print('Protocol : %s' % proto)

		lport = nm[host][proto].keys()
		lport.sort()
		for port in lport:
		    print ('port : %s\tstate : %s' % (port, nm[host][proto][port]['state']))
	print('-'*50)
	print('\n')

main()
