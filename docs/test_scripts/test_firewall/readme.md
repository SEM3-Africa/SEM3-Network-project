# Test of our firewall

This is a test of our vSRX firewall for Africa. The test will do pings and scan for open ports/services with nmap.

### How to test

![Picture](firewall_diagram.png)

To test the firewall for Africa, I have put my laptop on the physical PE5 SRX. I have made a connection from the firewalls ge/5 to my laptop on the 2001:0:5:200::0/64 subnet. 
Then i have made a script which will ping and scan ports on the firewalls 70::1 interface. This is also where all incoming traffic to our network will come. 

### Output of the test

![Picture](firewall_test.png)

In the first part of the output it pings the interface of the firewall. The next part it uses nmap to scan for ports. Port 22 is for SSH, 80 for HTTP and 179 is BGP. 

Link to script is [here](https://gitlab.com/SEM3-Africa/SEM3-Config-FIles/blob/master/test_scripts/test_firewall/ports-ping-fw.py)