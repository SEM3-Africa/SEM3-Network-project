# Test of management vSRX

This is a test of our management vSRX. The test will do pings and scan for open ports/services.

### How to test

To use this test script you have to be on schools network. Then i have made a script in python which scan ports with nmap and pings the given interface. 

### Output of the test

![Picture](mgmtsrx_test.png)

In the first part of the output it pings the interface of the management vsrx. The next part it uses nmap to scan for ports. 

Link to script [here](https://gitlab.com/SEM3-Africa/SEM3-Config-FIles/blob/master/test_scripts/test_mgmt_srx/ports-ping-mgmtsrx.py)