## Business Continuity Plan

The purpose of this document is to deliver a set of procedures and instructions on how to continue the business and it's operations in the event of any failure or disaster outside the companies control - and as such will serve as a general guideline on how to handle things.

#### Crisis Control Unit - Members

Members listed in the table below are apart of the emergency response team and are trained with the capability to handle any crisis that could hit the business.

#### In case of emergency

In case of emergency in regards to the project then the following people should be contacted, most notably the project manager should be the first port-of-call for anything emergency regarding the project - each are reachable via their email address posted below.

| Team   | Member Name    | Email               |
| ------ | -------------- | ------------------- |
| Member | Kyle McShame   | kyle0055@edu.eal.dk |
| Member | Kasper Jessen  | kasp6851@edu.eal.dk |
| PM     | Bo Mikkelsen   | boxx1483@edu.eal.dk |
| Member | Martin Nielsen | mart074a@edu.eal.dk |
| Member | Tim Lyder      | timx1571@edu.eal.dk |

Once a project member has been called to the scene and took action of any kind they should update the following table with details relating to the incident and what steps where took to fix it - in the event that the issue was not able to be fixed it should be noted and an alternative proposed in order to reach functionality.

#### Incident Table

| Date     | Team | Description of changes          | Member  |
| -------- | ---- | ------------------------------- | ------- |
| 29/08-18 | IT   | Example of a change in this box | Example |
|          |      |                                 |         |
|          |      |                                 |         |
|          |      |                                 |         |

<!--pagebreak-->

### Purpose and scope

The goal of ths document is to prevent any type of loss in the company and to minimize the impact of the business at any crisis like:

* Reduce the steps of decisions made when a crisis hits.
* Notify the right person with best knowledge on the actual crisis.
* Try not to run into a trial and error situation to fix the problem.
* To be prepared to a crisis at any kind with this document.
* Respond fast to an actual crisis or emergency situation.
* To rapidly recover the services to clients or servers.
* Rapidly to resume to normal business state.

### How to use

The plan should only be in case of an emergency mentioned in the "in case of" section.

If an emergency happens contact the CCU members listed in the first lines of this document. 

![Flowchart](https://gitlab.com/SEM3-Africa/SEM3-Network-project/blob/master/docs/Business_Continuity_Plan/Fire.png "Fire.png")

### In case of

* When a critical situation like when someone is messing with the blades in the server and the system goes down.  
    * Are we able to spin up a new syste with the backups of the ESXi

* Physical damage to the server.  
    * Spinning up the ESXi on the backup server.

* In case of downlink/uplink goes down
    * Create another conection to another place.

* In case of cooling goes down in the server room.
    * Temporary spin up another server another place.



### How to check the BCP

Once a month the plan should be revised and tested to see if it works as intented. When a BCP test have been tested and reviewed should the person who did it, insert the date of the test and insert comments if something needs to be cahnged, and the signature of the Member who did it.   

The member who made the test have to insert the date the next test have to be done into the table below.

* Step 1
    * Check The companys monitoring system (LibreNMS) for any erros / alerts
        * If there are alerts on LibreNMS, create a issue on gitlab to fix the alert.

* Step 2
    * Read whole doucment
        * If any inconsistency occur in the document please check up on the differences.

* Step 3
    * Do this
    * And this

| Test date | Comments to the plan                  | Signatrue of tester |
| --------- | ------------------------------------- | ------------------- |
| 29/08-18  | Comments for the test have to be here | Example             |
| 29/09-18  |                                       |                     |
|           |                                       |                     |
|           |                                       |                     |