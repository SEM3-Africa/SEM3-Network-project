# Business Continuity Plan

    Authors             : Kyle Mcshane & Martin Nielsen
    Purpose             : Steps taken in order to continue business operations in the event of failure.
    Date of Creation    : 24-09-2018

## Version Control

| Revision | Date       | Comments                                                                  | Changes By     |
| -------- | ---------- | ------------------------------------------------------------------------- | -------------- |
| 1.0      | 24-09-2018 | Created document and uploaded placeholder information needs more revision | Martin Nielsen |
| 2.0      | 05-10-2018 | Created initial document to layout how to handle physical server access   | Kyle Mcshane   |

## Purpose & Scope

The purpose of this document is to deliver a set of procedures and instructions the business must take in order to recover from emergencies and incidents, and as such should be employed in the occurrence of such matters. The aim of the document is also to deliver a complete recovery plan and process in order to maintain the businesses functionality in regards to the network infrastructure being built.

In short the scope of this document can be shrunk to the following 

* Reduce and limit the amount of steps and decisions made when a crisis hits - to deliver a fast and rapid response.
* To be able to Notify the right person(s) with best knowledge and insight regarding the actual issue.
* To be prepared in the event of a crisis/incident/emergency at any kind with this document.
* Deliver instructions to Respond rapidly to an actual crisis or emergency situation.
* To rapidly recover services to clients and/or servers.
* Aim to Rapidly restore the functionality to a normal operating-business state.


## Incident Response Unit

In the event of an emergency or incident it is the responsibility of each listed group member to resolve the issue and each may be contacted by the information laid out in the table below. It is important to note that all members have equal responsibility relating to the project and contacting them should be made in any order - Each member should serve as the first responders to any such matter in relation to the project or it's services.

### Contact Table

The following table contains contact information for each of the team members.

| Team   | Member Name    | Email               |
| ------ | -------------- | ------------------- |
| Member | Kyle McShame   | kyle0055@edu.eal.dk |
| Member | Kasper Jessen  | kasp6851@edu.eal.dk |
| PM     | Bo Mikkelsen   | boxx1483@edu.eal.dk |
| Member | Martin Nielsen | mart074a@edu.eal.dk |
| Member | Tim Lyder      | timx1571@edu.eal.dk |

### Incident Table

In the event of an emergency and/or incident each occurrence must be logged as to provide an accurate tracking of issues so we can adapt & change the BCP to better encompass these issues.

Once a project member has been called to the scene and took action of any kind they should update the following table with details relating to the incident and what steps where took to fix it - in the event that the issue was not able to be fixed it should be noted and an alternative proposed in order to reach functionality.

| Date | Devices | Description of Incident | Total Downtime | Responder(s) | Steps Taken |
| ---- | ------- | ----------------------- | -------------- | ------------ | ----------- |
|      |         |                         |                |              |             |


### Notable incidents

It is important to note that this part of the document is considered live and should be updated and maintained in order to provide correct and relevant information relating on incident handling, as not every issue can be covered due to the limitations of this document we should try to maintain a list of issues that are likely to affect us.

* When a critical situation like when someone is messing with the blades in the server and the system goes down.  
    * Are we able to spin up a new syste with the backups of the ESXi

* Physical damage to the server.  
    * Spinning up the ESXi on the backup server.

* In case of downlink/uplink goes down
    * Create another conection to another place.

* In case of cooling goes down in the server room.
    * Temporary spin up another server another place.