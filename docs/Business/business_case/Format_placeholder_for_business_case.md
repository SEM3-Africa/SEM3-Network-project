# ***Seven continents Business Case***

Group members involved: Bo Mikkelsen, Kasper Jensen, Martin Kiel, Tim Nikolajsen and Kyle Mcshane.

Gitlab project link (Mangement): [**Gitab**](https://gitlab.com/SEM3-Africa/SEM3-Network-project)

<!-- pagebreak -->

## **Preface**
*This document is written for the purpose of having overview on the business side of the project, without have an technical approch to it. This document should be revisted and rewritten doing the project, with possible change of plans.*
*This document is written for the porpuse of having overview on the business side of the project, without have a technical approch to it. This document should be revisted and rewritten doing the project, with possible change of plans.*

*This document should be used, as a reminder of policies or procedures according to the project. This should also be a pointer to how the project should be structured in our group.*

*This document will have analysis of strategy by having risk assesment, in regard of how to deal with certain predicted situations.*

<!-- pagebreak --> 

## Table of contents 
- [***Seven continents Business Case***](#seven-continents-business-case)
  - [**Preface**](#preface)
  - [Table of contents](#table-of-contents)
  - [**Revision** **History**](#revision-history)
  - [**Executive briefing**](#executive-briefing)
    - [Recommendation](#recommendation)
    - [Summary of Results](#summary-of-results)
    - [Decisions to be taken](#decisions-to-be-taken)
  - [**Introduction**](#introduction)
    - [Business Drivers](#business-drivers)
    - [Scope](#scope)
    - [Financial Metrics](#financial-metrics)
  - [**Analysis**](#analysis)
    - [Assumptions](#assumptions)
    - [Costs](#costs)
    - [Benefits](#benefits)
    - [Strategic Options](#strategic-options)
  - [Opportunity costs](#opportunity-costs)
  - [Excepted Risks](#excepted-risks)
- [Cost Savings](#cost-savings)
  - [**Equipment use and costs**](#equipment-use-and-costs)
    - [Hardware requirement list](#hardware-requirement-list)
      - [Network Hardware](#network-hardware)
      - [Server Hardware](#server-hardware)
    - [Device Datasheets](#device-datasheets)
- [**Conclusion**](#conclusion)
- [**Estimated Project cost**](#estimated-project-cost)
___
<!-- pagebreak -->

## **Revision** **History**
| Date     | Version | Changes                                                              | Page | Name       |
| -------- | ------- | -------------------------------------------------------------------- | ---- | ---------- |
| 26-08-18 | 1.0     | **First** **Document** **draft**                                     | n/a  | Bo         |
| 27-08-18 | 1.1     | **Document Changes to Opportunity**                                  | 5    | Bo         |
| 28-08-18 | 1.5     | **Added Cost saving section, with case**                             | 5    | Bo         |
| 31-08-18 | 1.6     | **Added Equipment use and costs**                                    | 6    | Bo         |
| 03-08-18 | 1.7     | **Reformatted document**                                             | n/a  | Kasper, Bo |
| 05-12-18 | 2.0     | **Added last placeholders and addtional risks, for intial document** | n/a  | Kasper, Bo |
| 05-12-18 | 3.0     | **Intial document**                                                  | n/a  | Bo         |
___
<!-- pagebreak -->

## **Executive briefing**

<i>This document will be covering the various business reasons for the intial scope of the project and the business benefits it can possibly create for the company. It will cover the drivers for this project profit wise. Also recommendation for cost savings and opportunities. <sup>1</sup></i>

### Recommendation
The recommeneded data-center moving, instead of using madagascar as one of the linked sites, instead Luanda in Angola. The reasoning behind the change can be found [HERE](#cost-savings). Recommendation would be to aviod these requirement various network technologies, instead desire the consultents best solutions that would finish the project quicker and smoother.

The internal teamwork between the sites, for creating a common backbone did not work out too well. Our groups recommendation is to have individual groups for each site and one group assigned to the backbone.

### Summary of Results
The results project turned out be a success without any major constrains that affected the end results. The opportunities turned out to be a great investment, and change for the overall project. The business drivers was fullfilled, as espected on Africa.

### Decisions to be taken
A lot of design decisions were taking doing the intial setup of the network. The equipment of software greatly changed the intial design of the network. 

The decisions to move the server site of madagascar to Angola Luanda greatly saved us cost of expenses and there for was a huge factor of saving money, and having a much better outcome overall.
___

<!-- pagebreak -->

## **Introduction**

### Business Drivers

The case of this project will be to devlop a new infastuctor in three countries as sites in Africa. The continent will then be connected to 6 other Contitental datacenters, which will be manged by the companies other consultents. This will expand our connection vastly, even to the ends of Africa, where we currently have no datacentors in place. Africa has gotten more attraction, over the few years. This is a case of their technology is growing, and the costs on placing centers are usally in the lower end. We want to act quickly, to try to make a monopoly on networking in Africa.

### Scope
Our sites for this project will be placed thorughout Africa in zones of:
* **Johannesburg (hub/PE)**
* **Lagos**
* **Luanda** (***Moved to Luanda in Angola from Madagascar***) 
    (see [Cost Savings](#cost-savings))

Keep note that theese sites are subject to change, for business benefits or technical connection wise.

### Financial Metrics

This will give us a great deal of return investment, because the net cash flow will increse, with being a reliable and secure connection site. Earnings per site is not concerned at this momment, but will be added for later inspection. Africa has very few big IT systems, that deliver reliable service. Therefore we will have a great marketshare as a competitor in the industry at Africa.
___

<!-- pagebreak -->

## **Analysis**
### Assumptions
The company as a business has a specified need to advanse our Information Technology infrastucture. Our connection sites, are limited and with this project, we will reach global connectivity. This will intruduce our customers/consumers to a world-wide scale. This will increse our scale-ability and return financially as improvement.
### Costs
The costs of this project will be 84 days of implementation for our site, with a 600kr per hour vage. (See [**Estimated Project cost**](#estimated-project-cost)). The 84 days will be an absolut dead-line and can not be exceeded, the site will be closed and cannot be connected if requirement are not implemented at the time. This also include documentation not set.
Addtional cost are added for equipment/hardware to make the site/project doable. It can be found in the [**Equipment use and costs**](#equipment-use-and-costs).

### Benefits
The business benefits of the project, will highly be the scaleability, and will join the global market in information technology infrastuctor. We will cover Africa, which is a continent of not many nor reliable datacentor is built/connect to. Our datacentors will have sites throughout strategic positions in Africa, and give us an financial benefit, and even monopoly is a possiblility.
### Strategic Options
Solveing this problem will be done by having technical teams in all the others site implementing their datacentors at 6 other continent (Europe, U.S.A, Australia, Asia, Russia, China and South America/Brazil). We will each create a backbone that can be connected together, and if each of the teams fail solving the problem, the continent will simply be discarded.

## Opportunity costs
<Placeholder for opportunity costs>

## Excepted Risks
__Risk__: Lack of time and manpower to complete tasks

__Identification__: To identify this risk, the PM should see a connection between main-tasks aswell as sub-tasks are not completed to the duo date.

__Assest__: This is a threat in regard of in can accour on many ocations

__Likelihood and impact__: The likelihood, is not well defined because it can't be foreseen easily. The impact of this risk will be quite significant to our part/state of the project. Our objectives will not be reached, and therefore not make it to the end project date, which cannot be moved. This will not only be a consern for us, but also other continents not being able to connect to us.

__How to respond to the risk__: Longer workhours, ask the superviser for more manpower.

___
__Risk__: Other continents interfrastructor problems, so we can't connect.

__Identification__: Advertised or Discussed by the group, that they will have problems, having it done on the date.

__Assest__: This is a threat to the project, because not all continents will be able to connect.

__Likelihood and impact__: The likelihood of this will be quite high, there are 6 groups are not all are on the same level. Therefore will is a strong possiblility.
This is not a threat but an opportunity, because it does not delay doing our continets task/mission, but the Project as a whole. We will still be able to do our part, so this is not a threat to our mission.

__How to respond to the risk__: We can only respond by either helping, if we have the time and resources to help fix the issue. If we can't or must not, then we will just have to wait.
___

__Risk__: Lack of knowledge to fullfill all project requirements.

__Identification:__ To identify this threat the PM should see a connection between tasks not being completed and people having troubles with certain topics. 

__Assest:__ This is a threat in the regard of some of the requirements not being completly fullfilled, which means we cant meet the customers requirements for the project. 

__Likelihood and impact:__ The likelihood is pretty small because theres only 1-2 topics we havent worked with before. The impact wont be big because we can ask the teacher to present it for us.

__How to respond to the risk:__ We can ask each other in the group if we stumble upon dificult topics. If that doesnt help we can ask the other groups. If they dont know we can ask the teacher to present the topic for us. 

<!-- pagebreak -->

# Cost Savings
***This section will include the planning of changes to save expenses***

* **Opportunity**: Moving datacenter from Antananrivo (Madagascar) to Luanda in Angola.

* **Business financial beneficial reason**: Reasoning for moving the datacenter's location have a few listed below beneficial options.

**Electricity:** 
Madagascar as a country, is very poor and a less decired country by many. This means that their technology part is way, lower than many other countries. This is important because if we want to build a datacenter, as a site in such a place, electrical power is important to take into consideration.  Looking at some statistics from the Goverment of Madagascar. The power cost is about kWh ≈ 0,70-0,80 usd.<sup>2</sup> [Madagascar_goverment_site](https://bit.ly/2wtDdmz).
Compared to Luanda in Angola, where the electrical cost is significantly cheaper. Looking at the Angolas goverment statetics. The power cost is around kWh ≈ 0,059usd.<sup>3</sup> [Luanda_goverment_site](https://bit.ly/2whHAli). This means that the powercost can be a significant saving for us in the long shot. If we take the Madagascaren price as avergae of kWh it will be 0,75 usd. To show in procentage it will be $((0,75÷0,059)-1)*100 ≈$ $1171.20$*%*. That means that placing a datacenter in Madagascar would just by power expenses be 1171% more expensive, than placing it in Launda. This will save us huge amounts in the long run.

**Transport expenses:**
Placing the datacenter in Madagascar will, provide some addtional costs to consider. Madagascars location is abroad of Africa, meaning that equipment will be troublesome transporting and cost addtional. First I will focus on personal transportasion to Madagascar compared to Luanda in Angola. I will use **Economy class** for comparison, with both price and time. I will use (https://skyscanner.net) for comparison on prices. If we take the transportation as our personal is staffed in Odense, Denmark. We will take **Billund airport** as reference because it has flights to both destinations (*tickets are one-way*). The table below show the numbers for single transport:

<u>**Madagascar(Antanrivo) from Billund**</u>

| **Type**              | **Transport time average**        | **Price of flight trip** |
| --------------------- | --------------------------------- | ------------------------ |
| ***Best Match***      | 19 hours and 10 minutes (average) | $8.841 kr.$              |
| ***Cheapest Flight*** | 50 hours and 55 minutes (average) | $6.999kr.$               |
| ***Fastest Flight***  | 16 hours and 18 minutes (average) | $18.769kr.$              |

<u>**Angola (Luanda)**</u>

| **Type**                     | **Transport time average**        | **Price of flight trip** |
| ---------------------------- | --------------------------------- | ------------------------ |
| ***Best Match and fastest*** | 11 hours and 15 minutes (average) | $10.352 kr.$             |
| ***Cheapest Flight***        | 42 hours and 45 minutes (average) | $7.555kr.$               |

This shows that traveling to Madagascar, is a bit cheaper, but the travel to Luanda is a lot shorter and faster. This is nessecary, if we need a technicen, fast at physical sight, time is pretty important.
This is only for single person flight. We need hardware shipment costs aswell.
On flight hardware transport costs will be around 7 dollars for each kilogram to madagascar. While Angola Luanda will be around 4.86 dollars for each kilogram. So the price will be quite more expensive to transport in madagascar.

**Dangerous country**
The country has had some quite extreme epidemic episodes, with plague. Theese are still happening till today, and therefore I belive it is not recommended to have consultents risk, their health, by going down there and stay. Source : (https://en.wikipedia.org/wiki/21st_century_Madagascar_plague_outbreaks)

**Connection**
Launda is specificly chosen, for strategic position according to sea cables. The biggest sea cable (EllaLink), which covers Angola, straight to Luanda. This is a 72 TB/s interface cable, which is the fastest and most stable in Africa. This will be a advantage to place it close to, instead of Madagascars 1,2 TB/s seacable.
Link for source: [Sea_cables_africa](https://manypossibilities.net/african-undersea-cables/)
<!-- pagebreak -->
___
## **Equipment use and costs**
To achive this project, our site needs some specific equipment to be used. The equipment has been allocated to fit the exact business needs to have a resillient and stable system. The equipment prices have been, researched to fit the requirements of exactly our project.

### Hardware requirement list

#### Network Hardware

**This section describes the physical hardware required in order to run our network.**

| Device Name                       | Device Type   | Description                                              | Product Code      | Amount | Capex Cost      | Opex Cost     | Total Capex | Total Opex |
| --------------------------------- | ------------- | -------------------------------------------------------- | ----------------- | ------ | -------------- | ------------- | ---------- | ---------- |
| MX10                              | Router        | Chasis + 1xPowersupply                                   | MX10BASE-T        | x1     | 257 257.54 DKK | 25 725.54 DKK |
| MX5                               | Router        | Chasis + 1xPowersupply and MIC-3D-20GE-SFP               | MX5BASE-T         | x2     | 151 781.95 DKK | 15 178.19 DKK | 303 356.39 | 30335.63   |
| 10-Gigabit Ethernet MICs with XFP | 10 GB MIC EXP | Gigabit Ethernet mic with XFP                            | MIC-3D-2XGE-XFP   | x1     | 89 277.27 DKK  | 8 927.77  DKK |
| EX2200                            | Switch        | Cheapest 802.1 Switches that support LAG,lacp and 802.1q | EX2200-24T-4G     | x4     | 4 705 DKK      | 470.5 DKK     | 18820 DKK  | 1882 DKK   |
| SRX 1500                          | Firewall      | Gateway Firewall that will provide external AS access.   | SRX1500-SYS-JB-AC | x1     | 74 262.74 DKK  | 7 426.27  DKK |
| Power supply 400W AC              | Power supply  | Power supply for SRX 1500                                | jPSU-400W-AC      | x1     | 7 717.73 DKK   | n/a           |
| SRX 240                           | Backbone PE   | PE router for MPLS backbone connected to our site        | SRX240H2          | x1     | 7 569.01 DKK   | 756.9 DKK     |

**Total equipment cost:**


 $ 257.257,54 + 303.359 + 89.277,27 + 18.820 + 74.262,74 + 7.717,73 + 7.569,01 = 758.261 DKK $

**Opex cost:** $10 $%$ $ of $ 758.261 = 75.826$ $ DKK $

**Total:** $758.261 + 75.826 = 834.087$ $ DKK $

#### Server Hardware
| Device Name          | Device Type    | Description                 | Product Code         | Amount | Capex Cost | Opex Cost | Total Capex | Total Opex |
| -------------------- | -------------- | --------------------------- | -------------------- | ------ | --------- | --------- | ---------- | ---------- |
| Dell Precision T3420 | Workstation    | Workstation for each city   | Dell Precision T3420 | 3x     | 4300 DKK  | 430 DKK   | 12 900 DKK | 1290 DKK   |
| Dell PowerEdge R740  | Hosting-Server | Server for hosting services | Dell PowerEdge R740  | 4x     | 15800 DKK | 1 580 DKK | 63200 DKK  | 6320 DKK   |

**Total server + pc cost:** $ 12.900 + 63.200 =  76.100DKK $

**Opex cost:** $10 $%$ $ of $ 76.100 = 7.610$ $ DKK$

**Total:** $76.100 + 7.610 = 83.710 $ $ DKK $

### Device Datasheets

[MX5-10 Datasheet](https://www.juniper.net/assets/us/en/local/pdf/datasheets/1000597-en.pdf)<br>
[MX MIC](https://www.juniper.net/documentation/en_US/release-independent/junos/topics/reference/general/mic-mx-series-10-gigabit-ethernet-xfp-description.html)<br>
[EX2200 Datasheet](https://www.juniper.net/assets/us/en/local/pdf/datasheets/1000307-en.pdf)<br>
[SRX-1500 Datasheet](https://www.juniper.net/assets/us/en/local/pdf/datasheets/1000551-en.pdf)<br>  
[SRX-240 Datasheet](https://www.juniper.net/assets/kr/kr/local/pdf/datasheets/1000281-en.pdf)
___

<!-- pagebreak -->

# **Conclusion**

This project should be done in 84 days, and have great potential to be done within the time limit. This is according to the excepted risks involed in the project and threat that can occour. The project will provide a great deal of benefits to the company. This involes the scalebility of the Companies Information technology infrastructor into a global backbone that can connect to 6 continents which makes it global scale.

The benefits of such and implementation will be an advantage to the company, and will make it as a future secure system with automated structor that will make it reliable for years to come.

<!-- pagebreak -->

# **Estimated Project cost**
The project has a scope of 84 days, to be done and worked on. The salery is average 600 kr. per hour for each consulent on work days/hours. The estimated workhours per week is 8 hours, sense we are 6 consulents working, it will be estimated an amount, which can be found in the table below.
It contains the estimated daliy, weekly monthly and the estimated whole project.
Hardware cost will exceed the budget based on the workhours.
This is based on the OPEX which is 10% of the Capital Expenditure which will be the hardware total cost. The OPEX is used for support, operations on software and licenses.

$$ kr=kroner $$
$$ p=person $$
$$ h=hours $$
$$ d=days $$
$$ m = monthly(30d) $$
$$ time = period $$

| Estimated (*Period*) cost | Equation $ = (kr*p*time) $ | Total estimate for period |
| ------------------------- | -------------------------- | ------------------------- |
| *Estimated daliy cost*    | $600kr*8h*5p$              | $24.000kr.$               |
| *Estimated weekly cost*   | $600kr*8h*5p*5d$           | $120.000 kr.$             |
| *Estimated monthly cost*  | $600kr*8h*5p*m$            | $480.000kr.$              |
| *Estimated Project cost*  | $600kr*8h *5p*84d$         | $2.016.000kr.$            |

**Total cost for equipment, and man hours payment:**

Total project cost =  $2.016.000 + 834.087 + 76.100 = 2.926.187 DKK$ 

This is: (Man-Hours) + (Network-hardware) + (Server-hardware)

Risk budget 15% of full budget = $2.926.187 * 0,15 = 438.928DKK$

Change budget 5% of full budget = $2.926.187 * 0,05 = 146.309DKK$