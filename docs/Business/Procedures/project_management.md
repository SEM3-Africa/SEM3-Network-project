# Project Management

This procedure will aim to address the current method for the way this roject should be managed.

From 24/09/18 onwards we have chosen to have a single project manager who will oversee the entire project and aim to create a single and strucutred project mangement method, it is important to note that the project management work should not be dumped on this person as the role serves to be a conduit for the management and will have a final and authoritative say on decisions relating to project management.

As of 24/09/2018 we have chosen the following person to be the project lead till the end of the project

<b>Bo Mikkelsen</b>

# Duties

    Create a strucutred enviroment for the group to work in.
    Serve as a common source for help in decision making inrelation to the projects tasks & milestones.
    Set out meetings with the mentors when required.
    Hold meetings and take group decisions into account when making project decisions.

Some duties may not be listed above but we all should hold a common-sense model when relating to project mangement.