# Documentation Procedure

This document will aim to set to standard as to which we write documentation in the project.

## Introduction

This part of the documentation should always aim to set the tone and audience for the entire document and as such should be formatted in the following way.

### Audience & Purpose

___

The Document hould always start with a list of relevant information indented - this will include attributes such as.

1. Author
2. Date
3. Purpose
4. Audience

Written in the following format

    Author  :   Kyle Mcshane
    Date    :   25/09/18
    Purpose :   To set a standard for us to write documentation
    Audience:   Team members

This will make it easier to set the tone and purpose of the document - effectively delivering more specific and relevant material.

### Revision HIstory

____

In addition to this introdcution a revision history must be written in the form of a table, and should include the following attributes.

1. Revision Number (minor changes shall be 0.1+ and major changes 1.0+)
2. Date written in the following format DD/MM/YYYY
3. Comments (Should be relevant to what happend in the document when it changed)
4. Changed by (Author(s) of the listed changes)

This should deliver a table that should look someting like this.

| Revision | Date       | Comments                                                                      | Changes By   |
| -------- | ---------- | ----------------------------------------------------------------------------- | ------------ |
| 1.0      | 25/09/2018 | Created the document                                                          | Kyle Mcshane |
| 1.1      | 25/09/2018 | Minor changes such as formatting and spelling                                 | Kyle Mcshane |
| 2.0      | 25/09/2018 | Major changes such as new sections or old sections being rewritten or deleted | Kyle Mcshane |

### Sections

___

Each topic within the document should have its own section and be specific abou what you are talking about - addiotionally as the tone and purpose of the document as has already been set at the start of the document you will not have to specify it again.

Try to keep the sections in the document as relevant as possible this can be helped by following a structured plan such as.

1. Introduction
2. Technical Information
3. Limitations
4. Review
5. Conclusion