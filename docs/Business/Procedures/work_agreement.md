# Working Agreement

This project encompasses each of the following members and they have signed an agreement to adhree to these rules as of 24/09/18.

    Bo Mikkelsen    -   Project Manager
    Kyle Mcshane    -   Team Member
    Kasper Jessen   -   Team Member
    Martin Nielsen  -   Team Member
    Tim Nikolajsen  -   Team Member

Each member has signed the working agreement and a copy of this document can be found [here](signed-team-contract).

## Working Guidelines

1. Each member should aim to stick to the working hours set below - if these cannot be met notify each other member of the issue/problem/reason and then a decision will be made.

2. The offical communication channels for the group shall be strictly set to english in both Gitlab repositorys and the Riot.im chatroom - it is important to note that both of these communication channels are the offical method of sharing information within the group.

3. Each member should aim to work on a milestone or issue at all times, if no task has been assigned to a member they should pick an issue out of the pool on gitlab to work on or bring it up with the project lead and come up with a new issue to work on.

4. In the event of a milestone or issue being late the project lead should be notified so the date can be moved and the correct documentation be updated.

5. Each member should firstly work to achive each of the goals set out in the project - these are defined as minimum reqiurements for the project and then anything developed after that shall be treated as a sub-goal meaning things that are nice to have rather than an actual requirement of the project.

6. Every member should use relevant commit messages and aim to keep their copy of the repository up to date at all times so the members can syncronize information easily between each other - we will not settle for files only being stored locally as this is a group project and information should be accessable to all membmers at all times.

7. Disagreements within the group shall be resolved civily and we should aim to not create a hostile enviroment in the form of a disagreement it will be down to the other three members of the group to come up with a vote on the right course of resolution to take.

## Working Hours

Each member has agreed to the working hours set below - in the case a member cannot make it please refer to the [Absence Scheme](absence_scheme.md) for further proceedures.

| Working Day | Start Time | Break Time | Return to Work | Finish Time | Other times / Breaks |
| ----------- | ---------- | ---------- | -------------- | ----------- | -------------------- |
| Monday      | 08:30      | 11:30      | 12:15          | 14:00       | 0:30                 |
| Tuesday     | 08:30      | 11:30      | 12:15          | 14:00       | 0:30                 |
| Wednesday   | 08:30      | 11:30      | 12:15          | 14:00       | 0:30                 |
