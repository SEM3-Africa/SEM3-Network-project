# Absence Scheme

This document will set out the basic guidelines and procedures for attendence and absence throughout the project.
It is important to note that repetative breach of this document will result in some form of disapliniary action within the group which will be enforceable in levels to the point
where termination from the group will be possible.

## Attendence

Current attendence for the current project encompass the current working days listed below.

| Working Day | Start Time | Break Time | Return to Work | Finish Time | Other times / Breaks |
| ----------- | ---------- | ---------- | -------------- | ----------- | -------------------- |
| Monday      | 08:30      | 11:30      | 12:15          | 14:00       | 0:30                 |
| Tuesday     | 08:30      | 11:30      | 12:15          | 14:00       | 0:30                 |
| Wednesday   | 08:30      | 11:30      | 12:15          | 14:00       | 0:30                 |

It is important to note that we are all equals and if a member cannot make it to a session then notice should be provided with a minimum time of 30mins, a reason does not have to be given but members should not take this procedure to abuse as repeated use may result in an infraction if reason is not given.

Repeated infractions will result in a vote from the other project membmers and if a vote passes it will ultimately result in the offender being removed from the project.