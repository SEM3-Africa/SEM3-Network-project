# Business strategy
  
___
## **Procedures:**
  
  
#### Absence:
  
* 1. In regard of absence send a message to teammates on Riot and teachers on email.   
* 2. Tell the teammates if you are able to do the issues from home.
* 3. ??
* 4. profit
  
#### Mondays:
  
* 1. Every monday we take a look at our issues and discuss what should be done, if anyone needs help and how far we have come.
* 2. Every monday we create our progress report from the progress report template and then present it in Peters class.
* 3. The progress report will contain what we did last week, what we are gonna do this week and our project plan.
* 4. The PM will present the progress report.
  
#### Issues:
  
  
* 1. When closing an issue remember to write a commit comment. Forexample if it needs more work later on, or if it is done.
* 2. Remember to keep issues up to date.
  
#### Files etc:
  
* 1. When creating documents, files etc. spaces are not allowed use underlines instead
* 2. Physical devices must be clearly labelled Group 5 - Africa.
  