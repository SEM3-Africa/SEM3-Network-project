# Notes for hardware costs
=
* US pricelist is in Dollars. Dollars needs to be in 6 kroner per dollar. This one is confusing and long

* Look on Junipers Cheatsheats for how much Gbps.
Should calculate the hardware speed.
The two mx's are connected to one SRX. If the one mx has a speed of 100 Gbps and the other 40 Gbps. The SRX <u>**NEEDS 140 Gbps**</u> This also include the firewall. Stateless will make it require less speed compared to a statefull firewall.

* Is traffic growing, the same  or declining. Look 18-24 months ahead.
* 

## Hardware Requirements

## Hardware List

This section describes the physical hardware required in order to run the internal autoanomous system aswell as the devices needed to provide inter AS connectivity.

## Requirements

Below sets out the requirements of each device type requires for the project, as such all shopping done in the following section will be based on the specificed requirements of this table. 

| Equipment             | Quantity | Requirements                                                                            |
| --------------------- | :------: | --------------------------------------------------------------------------------------- |
| Routers               | 2        | 18x GE interface(s), Minimum 20Gbps, LAG LACP.                                          |
| Gateway Router        | 1        | 2x XE interface(s), LAG + LACP, 18x GE interface(s), 1x FE interface(s), Minimum 20Gbps |
| Firewall/MPLS Routers | 2        | Minimum 20Gbps, 2x XE interface(s                                                       |
| Switches              | 4        | Minimum 4Gbps, LAG + LACP                                                               |
| Work Station PCS      | x4       | Basic Work Stations                                                                     |
| Server Racks          | x4       |

### Juniper Devices (Networking Equipment)

Pricing for Juniper devies in this section pertains to the datasheet set [here.](https://fronter.com/eal/links/url_redirect.phtml?id=1198736.)

| Device Name                       | Device Type      | Description                                            | Product Code      | Amount | Apex Cost    | Opex Cost   |
| --------------------------------- | ---------------- | ------------------------------------------------------ | ----------------- | ------ | ------------ | ----------- |
| MX10                              | Router           | Chasis + 1xPowersupply                                 | MX10BASE-T        | x1     | 241500 DKK   | 24150 DKK   |
| MX5                               | Router           | Chasis + 1xPowersupply                                 | MX5BASE-T         | x2     | 284970 DKK   | 28497 DKK   |
| Gigabit Ethernet MIC with SFP     | MX MIC Expansion | Gigabit Ethernet MIC with SFP (E)                      | MIC-3D-20GE-SFP-E | x1     | 43470 DKK    | 4347 DKK    |
| 10-Gigabit Ethernet MICs with XFP | MX MIC Expansion | 2x10 GB MIC                                            | MIC-3D-2XGE-XFP   | x1     | 13498.44 DKK | 1348.84 DKK |
| EX2200                            | Switch           | Cheapest 802.1 Switches that support LAG               | EX2200-24T-4G     | x4     | 17608 DKK    | 1760.80 DKK |
| SRX 1500                          | Router - Gateway | Gateway Firewall that will provide external AS access. | SRX1500-SYSJB-AC  | x1     | 69717 DKK    | 6971.70 DKK |
| SRX 240                           | Router - MPLS    | Used as connection for the MPLS network                | SRX240H2          | x1     | 7096.86 DKK  | 709.68 DKK  |

### Network Device Datasheets

[MX5 Datasheet](https://www.juniper.net/assets/us/en/local/pdf/datasheets/1000597-en.pdf)<br/>
[MX10 Datasheet](https://www.juniper.net/us/en/products-services/routing/mx-series/mx10/)<br/>
[Gigabit Ethernet MIC with SFP (E)](https://www.juniper.net/documentation/en_US/release-independent/junos/topics/reference/general/mic-mx-series-gigabit-ethernet-e-sfp-description.html)<br/>
[EX22000 Datasheet](https://www.juniper.net/assets/us/en/local/pdf/datasheets/1000307-en.pdf)<br/>
[10-Gigabit Ethernet MICs with XFP](https://www.juniper.net/documentation/en_US/release-independent/junos/topics/reference/general/mic-mx-series-gigabit-ethernet-e-sfp-description.html)<br/>
[SRX 1500 Datasheet](https://www.juniper.net/us/en/products-services/security/srx-series/srx1500/specs/)<br/> 
[SRX 240 Datasheet](https://www.juniper.net/assets/kr/kr/local/pdf/datasheets/1000281-en.pdf)<br/> 

## Server Equipment

This section will detail the devices needed in order to operate and maintain the servers.

### Dell Equipment

All devices listed are products from Dell.

| Device Name          | Device Type  | Description                                                                                                                        | Product Codes | Amount | Apex Costs |
| -------------------- | ------------ | ---------------------------------------------------------------------------------------------------------------------------------- | ------------- | ------ | ---------- |
| Dell Precision T3420 | Work Station | [T3420](https://www.dell.com/en-uk/work/shop/workstations/precision-t3420/spd/precision-t3x20-series-workstation/xctop3420sffemea) |               | x4     | 12900      |
| Dell PowerEdge R740  | Servers      | [R740](https://www.dell.com/en-uk/work/shop/povw/poweredge-r740)                                                                   |               | x4     | 47400      |
