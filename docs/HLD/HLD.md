# High Level Design Document

*This document, is the official High Level Design*

## Table of contents

- [High Level Design Document](#high-level-design-document)
    - [Table of contents](#table-of-contents)
    - [Revision History](#revision-history)
    - [Authors](#authors)
    - [Introduction](#introduction)
    - [Overview](#overview)
    - [Internal Africa (Continent) Site](#internal-africa-continent-site)
            - [Inner-site Routing](#inner-site-routing)
            - [Sites](#sites)
                - [Johannesberg](#johannesberg)
                - [Lagos](#lagos)
                - [Luanda](#luanda)
                - [Mgmt_srx](#mgmtsrx)
                - [Site Security](#site-security)
            - [General Network](#general-network)
            - [Network Management](#network-management)
        - [MPLS External Site (Backbone Network)](#mpls-external-site-backbone-network)
    - [Protocols](#protocols)
    - [Storage Configuration](#storage-configuration)
    - [Services](#services)
    - [Equipment](#equipment)
            - [Devices / Virtual machines](#devices--virtual-machines)
    - [Security](#security)
    - [IP Layout](#ip-layout)
    - [Naming Scheme](#naming-scheme)

## Revision History

| Revision | Date          | Comments                                                        | Changes By                     |
| -------- | ------------- | --------------------------------------------------------------- | ------------------------------ |
| 1.0      | 01-09-2018    | Initial draft of the document - generalised notes for later use | Tim Lyder                      |
| 2.0      | 19-11-2018    | Document Revision / Major Changes to sections and content       | Kyle Mcshane & Bo Mikkelsen    |
| 2.1      | 20-11-2018    | Minor Changes and formatting                                    | Kyle Mcshane & Bo Mikkelsen    |
| 3.0      | 27/28-11-2018 | Expansion of overview and additional sections                   | Kyle Mcshane                   |
| 4,0      | 03-12-2018    | More changes to sections with relevant information              | Kyle Mcshane                   |
| 4.1      | 04-12-2018    | Changed protocol and standards for customers needs              | Bo Mikkelsen                   |
| 5.0      | 04-12-2018    | Added the docker services and a small description               | Martin Nielsen                 |
| 5.1      | 04-12-2018    | Added storage configurations.                                   | Martin Nielsen                 |
| 5.2      | 04-12-2018    | Added devices and more services                                 | Martin Nielsen                 |
| 5.3      | 04-12-2018    | Added firewall description under Security                       | Tim Nikolajsen                 |
| 5.4      | 04-12-2018    | Added Lagos and Luanda router description                       | Martin Nielsen & Kasper Jessen |
| 5.5      | 05-12-2018    | Added MGMT_SRX description with details on functionallity       | Bo Mikkelsen                   |



## Authors

This project has been designed and theorized from the following members.

| Name           | Email               | Role               |
| -------------- | ------------------- | ------------------ |
| Bo Mikkelsen   | boxx1483@edu.eal.dk | Project Manager    |
| Kyle Mcshane   | kyle0055@edu.eal.dk | Project Consultant |
| Kasper Jessen  | kasp6851@edu.eal.dk | Project Consultant |
| Tim Lyder      | timx1571@edu.eal.dk | Project Consultant |
| Martin Neilsen | mart074a@edu.eal.dk | Project Consultant |

<!-- pagebreak -->

## Introduction

This document was creted in order to lay out the foundation and expectations of the proposed network we aim to build for the company "Booming Buisness Bloopers" - This project was undertaken in collaberation of five students from the UCL (Erhvervsakademi og Professionshøjskole) class of IT Technology (Networking Specilization) 2018-2019.

We will aim to put forward an insight into each design decision made throughout the project and make it a priority to use best effort and consideration to build the most functional and efficent network that we can deliver.

<!-- pagebreak -->

## Overview

The proposed network by the client has been through different iterations and changes in order to make the best most functional network we can deliver and as such since we have opted to host the netwrk in a virtual enviroment on the ESXI hypervisor the scope of the network had to be changed and some design decisions had to be made in order to fit the requirements - all decisions relating to these decisions were first consulted with the client.

The design requirements of the network required that the infrastucture was built and located at three seperate sites throughout the continent of africa - most namingly three sites known as Johannesberg/Lagos/Luanda from these sites we aim to deliver a zone of high avaidability to the network, however as the infrastructure of these sites has yet to be built we have taken the decision to present the network in a virtual enviroment using bare-metal hypervisors namingly the ESXI product from VM-ware to host and manage the network devices we require.

With the use of the ESXI Hypverisor we are able to host the proposed network in a virtual enviroment to provide a proof-of-concept network and demonstrate the network is functional as described.

The following diagram serves as a overview and reference of how the proposed network will be implemented and look.
____

![<i>High Level Diagram - Africa</i>](HLD_Full_-_No_IP.png)

## Internal Africa (Continent) Site

The network will be hosted on the ESXI Hypvervisor that is running on a server blade and as such we will virtualize each site and host them in one location for the proof of concept.

The core of the network is built between three MX series routers - which we have chosen to suit then needs of the network and to also provide a level of scalability in the network as the needs of the network now may be outgrown. We have chosen the MX Series routers for use primarilly as they match all the protocols and standards we require for use within the network such as their interface speeds and scalability and the level of upgradability they provide. 

Whilst Lagos/Luanda are cruical sites within the network we have taken the decision that we only require the MX5 routers for these sites as the level of demand is lower than the Johannesberg site which we have chosen a more powerful MX10 router as to serve as the networks gateway out onto the WAN/MPLS Network.

Johannesberg serves as the gateway out into the wider MPLS site that connects the customers network to other continents sites and as such will be the heart of the proposed network - the site will be the centralized location for all our network traffic to reach out onto the WAN for this purpose we have opted to use a powerful MX10 router that will be capeable for high-deliver all our traffic to this node and utilize it as the gateway to the MPLS network, for this purpose we opted to use a power MX10.

For the security out onto the wider MPLS network we have opted to use an SRX firewall device to serve as the security gateway for the network and as such should protect the inner site from possible threats originating from the WAN, aswell as catching sensative information from leaking out.

#### Inner-site Routing

Routing in the network is handled by the three powerful MX series routers hosted at each site and as such have been configurated with OSPF to provide easy of use with scalabiltiy and 

The MX series is a range of powerful-high performance routers from juniper that can support a multitude of different protocols which will or may be used within the scope of the project moving forward - additionally we have chosen the second iteration of the series the MX10 to serve as our gateway on the network.

All traffic that desires to leave the continent must first be served through the MX10 - which means that the demand for network access is much higher on the Johannesberg site - which is why we have made the decision to use a more powerful router at this site.

Additionally the MX series are upgradeable and allow for scability on its interfaces and firmware which means that if the needs of our network will expand past the capabilitys of the MX10 we should be able to scale up with ease.

#### Sites

Each site within the AS has been configured with the dynamic routing protocol known as OSPF - OSPF provides us with a level of scalability and automation of link discovery - it does this by handling the discovery and establishment of links between network devices via the use of network control messages sent to a multicast address associcated with the protocol - it is from here that nodes listen and communicate on the network in order to dynamically establish connectivity with each other. With this method we have provided a way of adding to the network in the event that the networks performance requirement increases.

##### Johannesberg

Serving as the network gateway the Johannesberg site has a much higher network demand than the other networking devices on the network - we have foreseen that this site will require a higher level of performance requirements and as such we have provisioned this and have opted to use a more powerful version of the MX series router the MX10 - this is to provide a higher avaidability and resillience in order to meet the required network demand.

Johannesberg hosts a From this site we are aiming to serve a USER-LAN where users can plug a workstation and recieve an automatic IP address from the DHCP server that resides on a seperate inferace and is relayed through to serve IPs to the workstations dynamically - this is a good practise as we can deliver ips automatically with out having to configure them.

##### Lagos

The Lagos router is one of main sites in the network which have a fileserver and a wokstation attached to it.

To get an ip address for the workstations do we have an DHCP relay installed on the router which forwards the DHCP request to the DHCP server that are located at the Johannesburg router. So the DHCP server will respond to the workstation with an ip address to use.
Luanda is connected with the 2 other routers with OSPFv3.

##### Luanda

At the Luanda site the primary function of node is to serve networks attatched to it -  Luanda router are one of the other main sites in the network which only have a workstation attached to it.

Like the lagos router are there installed a DHCP relay on the router which will forward the DHCP request to the DHCP server that is located at the Johannesburg router.

The luanda router is connected with the 2 other sites with OSPFv3

##### Mgmt_srx

This is the main router handling the whole mangement network. This router is the only router being directly on the schools network, by having a static ip address. This interface is the link that goes to the cisco switch in the server room. This is made for accessing the router from the schools network and connect through it that way. The router is based on a the Juniper VSRX Junos 15.1X49-D70.3, and is used as the jumphost and port forwarding security device. The srx has an indivial interface with a point to point connection to the following devices, **[Fw_srx, lagos, luanda and johan.]** theese are always the first interface mainly the ge-0/0/0.0. This subnet is based on ipv4 and isolated from the ipv6 subnet which are for the users. One of the interfaces on the mgmt_srx has an subnet that consist as a serverLAN for [services](#services), these servers has access to all devices for automation and monitoring etc. they also have access to the internet thorugh the mgmt_srx running nat to the school. The SRX has firwall policies to only allow the desired mangement traffic inbound, detailed description about  mgmt_srx security can be found [HERE](https://gitlab.com/SEM3-Africa/SEM3-Network-project/blob/master/docs/LLD/Security_Zones.xlsx).
The various port mappen can be found in [THIS](https://gitlab.com/SEM3-Africa/SEM3-Network-project/blob/master/docs/HLD/Machines_and_services_doc/inventory_of_ports.md) document.
The idea by jumping thorugh this device is to pass-through your public key to each device you jump. So the mgmt_srx's key is never used to access another device, but the jump users key instead.



##### Site Security


Security is handled on the site by the gateway router that handles all incoming and outgoing communication on the site - we have opted to use this gateway as a means of stopping sensative network traffic such as management, DHCP and telnet. The SRX Device is utilizing a list of policies and screens to catch these instances of traffic anda discard them. Additionally we are utilizing the firewall to secure the network from incoming traffic from the other sites around the world with the exception of allowing them access to the webserver in the DMZ zone.

#### General Network

Management of network devices and nodes on the network will be handled through the use of the SSH protocol which allows for bi-directional communication over an encrypted channel via the use of the rsa algorithm - this is done to make sure that all communication between the two devices is encrypted and plain-text displays of the configurations and password inputs are net exposed to anyone who may have compromised the line. Additionally we have opted to not use passwords in relation to the user accounts on the network devices and instead opted to use the public / private key system exclusively.

We are able to access the network for management on a seperate connection from the regular traffic, we have created a means of access via a SSH connection that proxies through the management SRX device we have - this means that we will initialize the connection to each device through the SRX device.

#### Network Management

Management of network devices and nodes on the network will be handled through the use of the SSH protocol which allows for bi-directional communication over an encrypted channel via the use of the RSA algorithm - this is done to make sure that all communication between the two devices is encrypted and plain-text displays of the configurations and password inputs are net exposed to anyone who may have compromised the line.

We have opted to not use passwords in relation to the user accounts on the network devices and instead opted to use the public / private key system exclusively - This is a security consideration we have chosen to implement for the use of the public/private key system allows for a more secure enviroment as an encyrpyion key system is a much more complex string of characters than a simple password for each user - additionally we can ensure that everybody who is connected for management adheres to this complex and secure method of management.

We are able to access the network for management on a seperate connection channel segmented from the regular transit traffic the network devices handle, we have the means of management access via a SSH connection that proxies through the management SRX device we have - this means that we will initialize the connection to each device through the SRX device.


<!-- pagebreak -->

### MPLS External Site (Backbone Network)

![<i>High Level Diagram - Africa</i>](HLD_Full_-_No_IP.png)

<!-- pagebreak -->

## Protocols

* **OSPF**
    * This protocol is refered to the [RFC2328](https://tools.ietf.org/html/rfc2328) and [RFC1247](https://tools.ietf.org/html/rfc1247) and has a documented standard stated there.
    * OSPFv2 is used as our dynamic routing protocol, on IPV4 in our mangement network subnets. This is the protocol that handles the interconnection between the follow router. *[mgmt_srx, fw_srx, johan, luanda, lagos]*.

    * The  first interface (ge-0/0/0) will be the mangement interface connected, to OSPFv2 in the area of 0.0.0.1. The area 0.0.0.1 is used to prevent interference with the OSPFv3 network, that area resides in Area 0.0.0.0 with IPV6 only.
        The mgmt_srx is the designated router, and all loopback are set to the physical unit 0.
        The loopback are set as passive in the mangement area to avoid advertising, to unwished destinations.
* **IPv6**
    * IPV6 is refered to the standard of [RFC2460](https://www.ietf.org/rfc/rfc2460.txt), [RFC3513](https://tools.ietf.org/html/rfc3513) and the new standard [RFC8200](https://tools.ietf.org/html/rfc8200). Which are standarized by the Internet Society
    * IPV6 is used through the whole production network. This is used because of the requirement of having it from the company. Also this secure us for future use, because we don't exhaust ips, and if ipv4 goes out of use we are secured.
    * We have an IP range of our site which is $ 2001:0:5::/48 $ which we need to device into segmented networks.
  
    * All point to point connections in our network has a /126 subnet mask. The reason for having a /126 compared to /127 is to have the use of ::1 and ::2 to connect. Nothing else is connected. This was due to juniper architecture, that allows ::1 and ::2 on /127 but, doesn't allow if with routing protocols is used for the interfaces/ips.
  
    * All interfaces inside the network except the ge-0/0/0 interface is set as family inet6. The loopback interface Lo0 on unit 0 is dual-stacked meaning is runs both ipv4 and ipv6 family. And both addresses are absolut(/32 and /128).
  
    * All users lans are placed in a /64 subnet, so it can scale well with high amount of device and user connections.

* **OSPFv3**
    * This protocol is refered as the standard by the internet society on the [RFC5340](https://tools.ietf.org/html/rfc5340) and [RFC5838](https://tools.ietf.org/html/rfc5838).
  
    * OSPFv3 is used as our dynamic routing protocol, for routing our ipv6 networks, together to communicate. The protocol interconnects all active interfaces running ipv6 family except ge-0/0/0 which is used for mangement.
  
    * OSPFv3 resides in area 0.0.0.0, it is place in this area considering we have no other ospf area are connected to our. This means that no OSPF backbone is required, and therefore can be run in any area desired. That said, the area 0.0.0.0 is a subject to change, for design changes or customer needs eg.
  
    * **Constrain** Right now we have a mangement network running on ipv4 in area 0.0.0.1 with OSPFv2 protocol. Duo to the ipv4 support in OSPFv3 as refered to this [RFC7949](https://tools.ietf.org/html/rfc7949)(IPv4 and IPV6 tranmission over OSPFv3), we can't have OSPFv2 and OSPFv3 to run in the same areas nor under the same protocol. This is because we don't want to advertise our mangemnet network to the production. It should also be noted, that they have to be seperated areas and protocol in case, we will implement dual-stack which is likely we will, the two protocol will conflict if configured in the same area.
  
    * **Routing policies** All our routing policies are configured on fw_srx. We export the default route, to the mpls bb, into OSPFv3 to allow traffic to flow out to the untrust zone. Also Direct route are exported, so we don't need to add the interfaces which aren't set in the OSPFv3 heriachy.


    * **VLAN**
        VLANs are used for the user lans on the different MX routers, one for each country and city.
    
    The order of Vlans is as follows.
    
    | Router | VLAN-id | Interface |
    | ------ | ------- | --------- |
    | Johan  | Vlan10  | ge-0/0/5  |
    | Lagos  | Vlan20  | ge-0/0/3  |
    | Luanda | Vlan30  | ge-0/0/3  |
    Each interface on the VMX's that contains a vlan has been set to vlan tagging, to allow the 802.1q IEEE standard. Vkabs are only used on the user lans, they are there to fill the customers requirement, and for scaling later on.
    They are fully routable between other lans.

* **DHCPv6**
    * This protocol is used to deploy an ipv6 on the host in the vlans dynmaicly. They all use a shared DHCP server running [ISC](https://help.ubuntu.com/community/isc-dhcp-server). This is the only service we could find that allowed DHCPv6 relay with multiple routers instead of router advertisement. The DHCP decides the subnet and ip address depending on a DORA follow by a unicast, to the DHCP server from a router (DHCP relay). Using EUI-64 would also be a possibility, but we decided to avoid the advertisement of mac addresses on devices. For some users this is a good privacy secure feature, to hide an identity.

* **NAT64**
    * Is needed to translate the IPv6 addresses from the Local Area Network to IPv4, and vice versa, which is used on the MPLS backbone to access the DNS servers and the webservers in the brazil site.

* **NAT44**
    * This protocol is used on the mgmt_srx, for forwarding traffic on the mangement network, to the schools gateway in order to access the internet. This is done by using the Default gatway and source natting to the schools gateway. This is used for updating/installing services on the server mangagement.

* **ICMP**
    * Is used for networking status and error messages concerning the network. This protocol is highly used for, testing connectivity.
* **UDP**
    * Used for sending ethernet packets without any handshaking to make sure the data is received
	* Specifically used in:
		* DHCP for sending out the IP addresses to the hosts.
		* Used by the quake server
		* OpenVPN
		* DNS
* **TCP**
   * Used for sending ethernet packets with handshaking to make sure the data is received. Used in EBGP and our SSH session. The protocol is also used for HTTP.
* **HTTP**
    * Is used for on the application layer of the TCP/IP stack for websites.
* **SSH**
  * Used for secure remote access to devices. This is used with public key encryption with RSA, all keys are 2048 bit and are stored under the user it belongs to. This allow us to use keys instead of password logins.
* **Netconf**
* The network management protocol, ansible and python can communicate with the routers and within the Netconf protocol on port 830 via. ssh. It is used for automating our operations. See more [HERE]()
* **SNMP**
    * We are using SNMP to monitor the routers and server within the "LibreNMS" server
* **EBGP**
    * The Path Vector based routing protocol used to connect the firewall to the MPLS Backbone, and read the MPLS routing tables. It is used by connecting our autonomous system, to the MPLS backbones autonomous system. 
* **DNS**
	* Used for connecting with names instead of IP addresses. The DNS servers we on the internal network is placed in the brazil site.

## Storage Configuration

The method we are storing the all the data for the serveral services, is made with an external datastore wich is placed in an other server. 
Which makes the ability for us to make it more reliable and easy to implement a backup server which have access to the datastore if the main server goes down.  

The San server is currently running in a FreeNAS environment which is a freeware software which is designed for "Network attached storage". The "FreeNAS" machine is running in an second ESXI machine that is running in the same server room as the main server.

## Services
	
* **Docker**
    * Docker is a service that can control many small environments on one machine. right now we are using docker to create alot of services like a "Web Server - Sql Server - LibreNMS - OpenVPN"  

* **Web Server**
    * The Web Server is running as an container inside docker and handle basic website that are accesible from outside.  

* **LibreNMS**
    * The LibreNMS service is running in a container in docker and are used to monitor the internal and external routers and the ESXI's.  

* **MySQL/MariaDB**
    * The SQL server is running in a docker container and are used for LibreNMS to store the graphs data. Can also be used for other services if needed.  

* **OpenVPN**
    * The OpenVPN service are running in a docker container and handles the VPN connection to the inside of the network.  

* **Portainer**
    * The portainer is running as an docker container and makes a user interface that can control docker containers and view status and usage of them.   

* **Ansible**
    * The Ansible service is running in an automation server which are used to collect the config files from the 3 internal routers once a hour and are uploading the config files if any changes have been done on the routers.

* **vCenter**
    * We are using a vCenter server which are handling more hosts as an cluster and can handle failover if one server goes down, then it will spin up the virtual machines on the selected backup server. vCenter is currently running as a virtual machine on the ESXI that are handling the "Network Attached Storage".

* **Nextcloud**
    * We are using the service nextcloud as an fileserver where you can up and download files from the workstation.

* **ISC DHCP**
    * ISC is a basic linux DHCP server that can handle IPv6 addresses for the workstation in the internal network. The ISC is running an a basic Debian machine inside esxi as a virtual machine.

## Equipment

| **Equipment**     | **Version/Model** | **Quantity** |
| ----------------- | ----------------- | ------------ |
| vSRX              | 15.1              | 2            |
| vMX               | 17.2              | 3            |
| ESXi              | 6.5               | 3            |
| vSphere/vCenter   | 6.5               | 1            |
| Acer Blade Server | AW2000h F2        | 3            |

* The vMXs will be used for routing
* The vSRXs will be used as a firewall
* The ESXi hypervisor will be used to run the virtual machines needed for this network.
* vSphere/vCenter is used to mange multiple ESXI's and to handle failover if the main server goes down.
* The blade server is where the ESXi will run with all VMs needed. 

#### Devices / Virtual machines

* **Docker machine**
    * The docker machine is the virtual machine that are based on debian that have the application docker installed and are handling all the containers.
* **Automation Server**
    * The automation servers is a debian machine which have the service ansible installed and are running some ansible playbooks with Cron job.
* **Fileserver**
    * The fileserver is Debian machine which are carrying the service "Nextcloud" 
* **DHCP Server**
    * The DHCP Machine is a standard Debian based virtual machine which are configured with the ISC service.
* **Firewall**
    * We have a firewall which is an vSRX running as a virtual machine on the ESXI server.
* **Management Router**
    * The Management router is a vSRX which is a virtual machine running on the ESXI server.
* **Workstations**
    * We have 3 workstations virtual machines which are acting like regular workstations, there is 1 workstation on each locatation in the network.  
    The workstations are running Linux-Fedora as the file system and have the minimal requirements for a Linux machine to run. and are running on the ESXI server.
* **Routers**
    * We have 3 vMX's that are located 1 on each location and they run on the ESXI server.


## Security
* System Security
    * Firewall
		* DHCP Requests can only be made from within the continent.
        * Telnet is only allowed from within the same continent and rejected from other.
        * Only members of this project group have access through the firewall.

	
	* User Authentication
		* SSH keys are used to authenticate our logins to the routers.
		* All remote access is only allowed via a Secure VPN connection.
  
    * Public Access
		* No public access is allowed except with a secure VPN connection and only for authorized users.
        * Physical Access to System
		* Physical access to any device, (vMX, vSRX or SRX240) is done in the server room which has a physical lock.
		* Access to the SRX240 MPLS backbone can only be accessed via the console port.
  
    * Disaster Protection and Recovery
		* To comply with the 99.999% uptime there will be no SPoF (Single Point of Failure) throughout the network.
  
    * System Monitoring
		* To monitor our system we use LibreNMS.
  
* Software Application Security
    * User Authentication
		* We use SSH keys to authenticate users logging in.
    * Electronic Data Transmission
    * Electronic Data Storage
  
* Data Protection
    * Protection of any hard copy generated by or for The Company that contains Personal
      Protected Information when hard copy is in a public area. COMPLY WITH GDPR!


## IP Layout

* There is no IP Scheme yet as IP addresses are given by the EUI-64 protocol.
* IPs available: 2001:0:5/48

## Naming Scheme
* The names of the routers are vMX-*"city"*.
    * Routers
        * vMX-Johan
        * vMX-Lagos
        * vMX-Luanda


* The workstations are named ws-*"city"*. 
    * Workstations
        * ws-johan
        * ws-lagos
        * ws-luanda