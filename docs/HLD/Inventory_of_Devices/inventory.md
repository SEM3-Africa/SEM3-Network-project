# Inventory of Devices

	Author	: Tim Nikolajsen
	Date	: 11/9/18
	Purpose	: Have an inventory of both physical and virtual devices in the network.
	Audience: Project Members

	
## **Revision** **History**
  
| Version	|  Date    | Changes                                  	| Name				|
| --------- | -------- | ------------------------------------------	| ----------------- |
| 1.0      	| 11-09-18 | First Document Draft         				| Tim Nikolajsen  	|
| 1.1	  	| 13-09-18 | Now Includes Physical Devices				| Tim Nikolajsen  	|
| 1.2	  	| 01-10-18 | Now Includes vSwitches						| Tim Nikolajsen  	|
| 1.3		| 29-10-18 | Updated									| Tim Nikolajsen	|
| 2.0       | 05-12-18 | Final Draft                                | Tim Nikolajsen    |


## Virtual Devices

| ## | **System Name** 	| **Virtual Device Type** 	| **Operating System**  | **OS Version** 	| **Date Installed** | **Purpose** 								| **IP Address** 				| **Notes** 				|
| -- | ---------------- | ------------------------- | :-------------------: | :---------------: | :----------------: | ---------------------------------------- | ----------------------------- | ------------------------- |
| 01 | Docker		   	| Container Management	 	| Debian		   	 	| 9.5  				| 05-09-18	   	     | Service Consolidation					| 192.168.50.10					| VPN, MariaDB,  |
| 02 | Johan-CP		   	| Router Control Plane	 	| FreeBSD		   		| 17.2r1	  		| 11-09-18	       	 | Router Config							| N/A							| 					 		|
| 03 | Johan-FP		   	| Router Forwarding Plane 	| FreeBSD          	 	| 17.2r1	  		| 11-09-18	       	 | Routing 									| 192.168.51.2					| 					 		|
| 04 | Lagos-CP		   	| Router Control Plane	 	| FreeBSD		   		| 17.2r1 	  		| 11-09-18	       	 | Routing Config							| N/A							| 							|
| 05 | Lagos-FP		   	| Router Forwarding Plane 	| FreeBSD          		| 17.2r1 	  		| 11-09-18	       	 | Routing 									| 192.168.51.10   				| 					 		|
| 06 | Luand-CP		   	| Router Control Plane	 	| FreeBSD		   		| 17.2r1 	  		| 11-09-18	       	 | Routing Config							| N/A							| 					 		|
| 07 | Luand-FP		   	| Router Forwarding Plane 	| FreeBSD          	 	| 17.2r1 	  		| 11-09-18	   	   	 | Routing 									| 192.168.51.6   				| 				 			|
| 08 | Ansible		   	| Automation Workstation	| Debian		   	 	| 9.5	 	  		| 25-09-18		   	 | Automation								| 192.168.50.20					| 				 			|
| 09 | MGMT-SRX01    	| vSRX             	 	 	| FreeBSD          	 	| 15.1X49-D70.3 	| 05-09-18       	 | VPN     									| 10.217.19.239   				| 		 			 		|
| 10 | ws-johan	   		| Workstation				| Fedora		   		| 28	 	  		| 14-09-18			 | Work    									| DHCP	(2001:0:5:80::100-110)	| Default Image		 		|
| 11 | ws-lagos	   		| Workstation				| Debian		   		| 9.5				| 14-09-18			 | Work    									| DHCP	(2001:0:5:50::100-110)	| Default Image		 		|
| 12 | ws-luanda   		| Workstation			 	| Fedora		   		| 28	 	  		| 14-09-18			 | Work    									| DHCP	(2001:0:5:60::100-110)	| Default Image		 		|
| 13 | DHCP-Server	   	| Server					| Debian		   		| 9.5	 	  		| 14-09-18		   	 | DNS & DHCP for Management network 		| 192.168.50.60					| Default Image	 	 		|
| 14 | dmz-web-server  	| Server					| Debian				| 9.5				| 29-10-18			 | Web server for external access			| 2001:0:5:90::2				| Default Image				|
| 15 | AfricaFW-SRX-1	| Firewall					| FreeBSD				| 15.1X49-D70.3		| 29-10-18			 | Gateway between Africa and MPLS Backbone | 192.168.51.14					|							|
	


## Virtual Switces

| ## | **System Name**  | **Connects** 											| **Notes***		|
| -- | ---------------- | ----------------------------------------------------- | ----------------- |
| 01 | Africa-MPLS	   	| vSRX15, Docker, Radius-Auto							|					|
| 02 | br-ext1			| Lagos-CP, Lagos-FP									|					|
| 03 | br-ext2			| Luand-CP, Luand-FP									|					|
| 04 | br-int			| Johan-CP, Johan-FP									|					|
| 05 | br-int1		    | Lagos-CP, Lagos-FP 									|					|
| 06 | br-int2		    | Luand-CP, Luand-FP									|					|
| 07 | dns-johan		| DNS6, Johan-FP										|					|
| 08 | fw-nic		    | AfricaFW-SRX-1, NIC0					    			|					|
| 09 | johan-africa	    | Johan, AfricaFW-SRX-1									|					|
| 10 | lagos-johan     	| Lagos-FP, Johan-FP									|					|
| 11 | luanda-johan	    | Luand-FP, Johan-FP									|					|
| 12 | luanda-lagos	    | Luand-FP, Lagos-FP									|					|
| 13 | p1p1	    		| Lagos-FP, Luand-FP, Johan-FP, AfricaFW-SRX-1, vSRX15  |					|
| 14 | Web-Client		| Routing Config										|					|
| 15 | ws-lan-johan		| Johan-FP, fedora-Johan 								|					|
| 16 | ws-lan-lagos	    | Lagos-FP, debiaN-lagos								|					|
| 17 | ws-lan-luanda    | Luand-FP, fedora-Luanda     							|					|
| 18 | fw-dmz  			| dmz-web-server, AfricaFW-SRX-1 						|					|
| 19 | fw_fxp  			| AfricaFW-SRX-1										|					|


## Physical Devices

| ## | **System Name** | **Device Type** 		 | **Operating System** | **OS Version** | **Date Installed** | **Purpose** 	            | **IP Address** 	| **Notes**                                         |
| -- | --------------- | ----------------------- | :------------------: | :------------: | :----------------: | --------------------------- | ----------------- | ------------------------------------------------- |
| 01 | ESXi_Prod       | Type-1 Hypervisor  	 | N/A          	  	| 6.5		  	 | 03-09-18           | VM Management 	            | 10.217.19.240   	| Production   	 	                                |
| 02 | SRX240		   | Router					 | FreeBSD		   	    | 12.1X46-D30.2	 | TBD			  	  | Provider Edge	            | 192.168.51.13  	|   		                                        |
| 03 | vCenter	       | Type-1 Hypervisor		 | N/A  		   	    | 6.5	 	  	 | 05-12-18			  | VM Management	            | 10.217.19.240		| Used for SAN server and management of vMotion.    |
| 04 | EX4200          | Level 3 Switch          | FreeBSD              | 12.1X46-D30.2  | 21-11-18           | Switching                   | N/A               | Connect ESXi's and vCenter                        |
| 01 | ESXi_Test       | Type-1 Hypervisor  	 | N/A          	  	| 6.5		  	 | 03-10-18           | VM Management 	            | 10.217.19.238   	| Testing   	 	                                |