# Inventory of Devices

This section will cover the physical and virutal aspect of devices within the system, and as such is subject to change as of 28/08/18

## ESXI_Blade_1

    ESXI Version - 6.5

    VM Status Format
    ----------------------------------------------------------------------------
    [ x ] - Down / Disabled
    [ o ] - Up / Live

## Placeholder for VMs

    This section will contain information relating to the virtual machines we will be utilising.

| System Name | Virtual Device Type | Operating System | Version | Date Installed | Purpose | Notes | IP Address |
| ----------- | ------------------- | ---------------- | ------- | -------------- | ------- | ----- | ---------- |
| ###         | ###                 | ###              | ###     | ###            | ###     | ###   | ###        |