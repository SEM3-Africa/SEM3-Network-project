# Inventory of ports

### mgmt_srx



| Portnumber | Service    | Description                                   | 
| --------   | ---------- | --------------------------------------------- | 
| 22         | SSH        | For SSH connections                           | 
| 80         | HTTP       | For webserver                                 |
| 161        | SNMP       | Monitoring on LibreNMS                        |
| 443        | HTTPS      | For webserver                                 |
| 943        | OpenVPN    | VPN connection Webinterface                   |
| 1194       | OpenVPN    | VPN connection Encryption                     |
| 3306       | MariaDB    | Database                                      |
| 5030       | AWX        | Ansible Tower Automation WebUI                |
| 8080       | LibreNMS   | Monitoring with LibreNMS                      |
| 8085       | Quake srv  | Quake webserver                               |
| 9000       | Portainer  | Management UI for Docker                      |
| 9443       | OpenVPN    | VPN connection Webinterface                   |
| 27960      | Quake srv  | Quake webserver                               |

