
    Author  :   Kasper Jessen
    Date    :   25/09/18
    Purpose :   To set up 2 factor authentication for the ESXi
    Audience:   Team members

We couldnt find any information about using Radius for the ESXi. Instead I find something interesting about Google Authentication for the ESXi.
https://labs.vmware.com/flings/esxi-google-authenticator

## What is Google Authenticator

Google Authenticator is a mobile app that implements 2 step authentication using the Time-based One-time Password algorithm and HMAC-based One-time Password algorithm. The authenticator provides a one-time password which users must provide in addition to their username and password to log into a Google service or third party applications like our ESXi. 

## Why we use Google Authentication

We use the 2 factor authentication to establish more security. Now only team members with the time based password are able to login to the ESXi via SSH. 

## Which devices

The Google Authenticator is installed on the ESXi. Then you need a smart phone with the app to recieve the time based code needed to login to via SSH.

## How to install

Download the the zip file from the link and upload the .vib file to the esxi datastore. Then run this command to install the authenticator:

esxcli software vib install --no-sig-check -f -v /path/to/the/vib

When this is done you can run the google-authenticator in the cli. This will ask you a couple of security questions and setup some codes. You will get a link to a bar code you can scan with the google auth app. Now you have your time based verification codes on your phone.

Paste in this command to set up to 2 step verification:

/etc/ssh/sshd_config 
ChallengeResponseAuthentication yes

Then these commands. This will make google auth the first security verification asked when you login:

sed -i -e '3iauth required pam_google_authenticator.so\' /etc/pam.d/sshd
sed -i -e '3iauth required pam_google_authenticator.so\' /etc/pam.d/login


Then this to restart the configs. 

/etc/init.d/SSH restart

This command will make sure that the configs still are working after reboot. 

sed -i -e '3iauth required pam_google_authenticator.so\' /etc/rc.local.d/local.sh

When SSH and ESXi shell is enabled you can disable the ESXi webclient.
To disable ESXi webclient: https://kb.vmware.com/s/article/1016039

