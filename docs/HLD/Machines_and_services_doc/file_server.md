# File server documentation

	Author	: Martin Nielsen
	Date	: 28/11/18
	Purpose	: Inform where the file server is running and what are used to get it running
	Audience: Project Members

	
# Revision

| Revision | Date       | Comments                                      | Changes By  	 |
| -------- | ---------- | --------------------------------------------- | -------------- |
| 1.0      | 28/11/2018 | Initial Document                              | Martin Nielsen |


# File server 

THe file server is a basic "Nextcloud" service that are running on a debian machine on the user lan Lagos where we are able to 
 
IP of the file server machine: 2001:0:5:50::200
                         Port: 80

## Purpose  

First of all is it a requirement for the project that we installs a fileserver to the network.  
All users of the network a able to connect to the fileserver and drop some files on it through a web client from either the another client as long as its connected to the network through the VPN.


## References

For documentation about docker and how we installed it, is all found on: https://nextcloud.com/