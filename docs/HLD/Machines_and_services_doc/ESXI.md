# ESXI Documentation

    Author   :   Kyle Mcshane
    Date     :   25/09/18
    Purpose  :   To deliver an explanation of why we use the ESXI Hypervisor
    Audience :   Generic

## Revision History

| Version | Date     | Notes                                                        | Changes by   |
| ------- | -------- | ------------------------------------------------------------ | ------------ |
| 1       | 24/09/18 | Created documentation draft                                  | Kyle Mcshane |
| 1.1     | 25/09/18 | More detailed information and specific audience as was added | Kyle Mcshane |

## Purpose Of Use

The ESXI hypervisor is a powerful tool that we have chosen to utilize within our project - As it is a hypervisor it provides us with the ability to host multiple unique &independant machines on a singular physical device - this is incredibly useful for our purposes as the majority of our machines and services will run virtually and having a common chasis which we can log into a monitor which machines are running is an incredibly powerful tool for us.

As the ESXI runs multiple machines and as such they all have been given an ethernet adapter to use on the device

## Where does it run?

It is important to understand that we intend to run the ESXI hypvervisor directly on a bare-metal machine, this is known as a level 1 hypervisor and allows for more efficent and effective management of resources for the hypervisor because it doesn't have to depend on the operating system it is nested in to allocate it resources.

The current version of the ESXI we are currently running is version 6.5 as it is the current revision on ESXI as of 24/09/18, Additionally we have installed the Hypervisor on the Intel Blade Server.