This shows which VM is connected to which portgroup and virtual switch.

**vMXs**

|**VM**|**Portgroups**|**vSwitch**|
|--|--|--|
| vMX-Johan-vcp         | br-ext-johan       | Web-Client     |
|                       | br-int-johan       | br-int         |
| vMX-Johan-vfp         | br-ext-johan       | Web-Client     |
|                       | br-int-johan       | br-int         |
|                       | mgmt-vsrx          | p1p1           |

|**VM**|**Portgroups**|**vSwitch**|
|--|--|--|
| vMX-Lagos-vcp         | br-ext1-lagos     | br-ext1         |
|                       | br-int1-lagos     | br-int1         |
| vMX-Lagos-vfp         | br-ext1-lagos     | br-ext1         |
|                       | br-int1-lagos     | br-int1         |
|                       | mgmt-vsrx         | p1p1            |

|**VM**|**Portgroups**|**vSwitch**|
|--|--|--|
| vMX-Luand-vcp         | br-ext2-luand     | br-ext2         |
|                       | br-int2-luand     | br-int2         |
| vMX-Luand-vfp         | br-ext2-luand     | br-ext2         |
|                       | br-int2-luand     | br-int2         |
|                       | mgmt-vsrx         | p1p1            |


**Other**

|**VM**|**Portgroups**|**vSwitch**|
|--|--|--|
| DNS4             | Africa          | Africa-MPLS            |
| DNS6             | Africa          | Africa-MPLS            |
| Docker           | Africa          | Africa-MPLS            |
| Radius-Auto      | Africa          | Africa-MPLS            |
| vSRX15           | vSRX_mgmt       | Web-Client             |
|                  | vSRX_mgmt       | Web-Client             |
|                  | mgmt-vsrx       | p1p1                   |
|                  | Africa          | Africa-MPLS            |
|                  | mgmt-vsrx       | p1p1                   |
|                  | mgmt-vsrx       | p1p1                   |

**Workstations**

|**VM**|**Portgroups**|**vSwitch**|
|--|--|--|
|ws-johan|ws-lan-johan|ws-lan-johan|
|ws-lagos|ws-lan-lagos|ws-lan-lagos|
|ws-luanda|ws-lan-luanda|ws-lan-luanda|

**Portgroups**

|**Portgroups**|**VLAN ID**|
|--|:--:|
|ae0-johan|1|
|ae0-lagos|2|
|ae0-luanda|3|
|ae1-johan|4|
|ae1-lagos|5|
|ae1-luanda|6|
|ae2-johan|7|
|ae2-lagos|8|
|ae2-luanda|9|
|Africa|11|
|br-ext-johan|12|
|br-ext1-lagos|13|
|br-ext2-luand|14|
|br-int-johan|15|
|br-int1-lagos|16|
|br-int2-luand|17|
|dmz-webserver|18|
|dns-johan|19|
|FW-MPLS|40|
|Management Network|22|
|mgmt-vsrx|23|
|vSRX_mgmt|24|
|ws-lan-johan|25|
|ws-lan-lagos|26|
|ws-lan-luanda|27|