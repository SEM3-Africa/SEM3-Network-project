# Management Network Documentation


## Introduction

This document will go through why we chose to have a management network aimed at project members.

	Author	: Tim Nikolajsen
	Date	: 26/9/18
	Purpose	: Inform about choice of having a management network
	Audience: Project Members

	
## Revision

| Revision | Date       | Comments               | Changes By     |
| -------- | ---------- | ---------------------- | -------------- |
| 1.0      | 26/09/2018 | Initial Document       | Tim Nikolajsen |
| 2.0      | 07/11/2018 | Updated a few features | Bo Mikkelsen   |


## Why

We are using a management network to have access to the network devices even if the users of the network do not.

## Which Devices

A vSRX is used as an SSH proxy to have access to the different routers.

The vSRX is also used for port forwarding to the docker instances on the docker machine.


## How is it used?

#### SSH Proxy

We are using the command "Proxycommand ssh -W %h:%p <hostname>" to SSH through the vSRX to the other devices in the network.

This configuration is part of each users SSH settings.

There is a config file located on gitlab containing all the settings which can be downloaded for use by the user.

#### Port forwarding

We are using port forwarding to gain access to the by going using the IP followed by the port number i.e. 1.1.1.1:9555

#### Firewall

The SRX is using it own firewall feature, to block out unwanted inbound traffic. It is set to flow mode, and only the ports used/forwarded are allowed statefully.
SSH passwords login are disabled, considering there always are 5 users, and two root users with a ssh-key assigned. It won't be a problem if somebody loses their key.


## References

[Docker Documentation](https://gitlab.com/SEM3-Africa/SEM3-Network-project/blob/master/docs/HLD/Machines_and_services_doc/Docker.md)