# Docker Documentation

	Author	: Martin Nielsen
	Date	: 26/9/18
	Purpose	: Inform about choice of having docker.
	Audience: Project Members

	
# Revision

| Revision | Date       | Comments                                      | Changes By  	 |
| -------- | ---------- | --------------------------------------------- | -------------- |
| 1.0      | 26/09/2018 | Initial Document                              | Martin Nielsen |


# Docker - Why we use it

Docker is an application that runs containers inside an environment running on a workstation machine.  
in our case are we using a Debian machine for that. we are using docker to save space and resources on the server and to have all our services in one place.  
 
IP of the docker machine: 192.168.50.10

 
# Containers

Below is all the containers we have in the docker environment right now and will be updated when new stuff is being added to the engine.

Every container will be listed with a couple of lines what it does and wy we have it.

## Nginx - WebServer

Nginx is a simple webserver where we can host HTML and PHP files to the public when they access our public ip.

#### Configuration
Container name: Nginx  
Created: 2018-09-14 09:45:36  
Access-IP: 192.168.50.10  
Docker-IP: 172.17.0.5   
PORT: 80,443


## MariaDB

MariaDB is a platform that runs an upgraded MYSQL platform and are used to stor data from our LibreNMS server to keep track on graphs and other stuff.

#### Configuration
Container name: MariaDB  
Created: 2018-09-12 12:34:29  
Access-IP: 192.168.50.10  
Docker-IP: 172.17.0.3  
PORT: 3306

## Ansible Tower

Ansible tower is an service we use to run ansible playbooks to example pull and push configuration from our vmx's on esxi to fast send configuration to the routers.

#### Configuration
Container name: Ansible-Tower 
Created: 2018-09-24 12:47:32
Access-IP: 192.168.50.10  
Docker-IP: 172.17.0.6  
PORT: 8090

## LibreNMS

The LibreNMS service is where we are getting graphs and status about whats happening on the vmxs and we can see if something wierd is gonna happen 

#### Configuration
Container name: LibreNMS
Created: 2018-09-12 10:57:50  
Access-IP: 192.168.50.10  
Docker-IP: 172.17.0.2  
PORT: 8080

## OpenVPN

OpenVPN is our vpn service so we can make a encrypted VPN tunnel into the mgmt network and to configure from the inside without need of port forwarding.

#### Configuration
Container name: OpenVPN  
Created: 2018-09-13 12:23:07 
Access-IP: 192.168.50.10  
Docker-IP: 172.17.0.2  
PORT: 1194,994,9443  

## Portainer

Portainer is a Docker container that makes a webserver instance where we can access a GUI on a webserver to manage basic stuff on the docker environment like create and remove containers and add more ports assigned to the container.

#### Configuration
Container name: Portainer 
Created: 2018-09-11 13:16:58  
Access-IP: 192.168.50.10  
Docker-IP: 172.17.0.4  
PORT: 9000

## References

For documentation about docker and how we installed it, is all found on: https://www.docker.com/