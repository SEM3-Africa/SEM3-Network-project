# UCL Semester 3 IT Technology (Networking) - Network Project

This project was created as a collaborative effort for the purposes of the Networking Semester 3 hand-in at UCL Erhvervsakademi og Professionshøjskole.

# Final report is found [HERE](https://edueal-my.sharepoint.com/:w:/r/personal/mart074a_edu_eal_dk/_layouts/15/Doc.aspx?sourcedoc=%7B7CFB98A4-9673-40E5-927E-3C4616CAAB3E%7D&file=Document%201.docx&action=editnew&mobileredirect=true&wdNewAndOpenCt=1543821546365&wdPreviousSession=3d5b12de-d3d2-4c18-b303-2f8f271b31e1&wdOrigin=ohpAppStartPages)

## Seven Continents Project - Team 5 - Africa

    * Project Name - Seven Continents - Africa
    * Customer - BBB (Booming Buisness Bloopers)
    * Start Date - 20/08/2018
    * End Date - 14/12/2018
  
## Overview

The project will consist of consulting teams each derived from members of the UCL Semester 3 Networking students, as such this repository will only pertain to the members of the African Continent Team [5]

Our consultant team has been alllocated to build and configure our infastructure in specific cities of the continent. As such the African region has been assigned the following cities for the data centers to be located.

    * Johannesburg (hub/PE)
    * Lagos
    * Luanda (Angola)
  
From these data centers we will aim to provide external connectivity to other contients via the use of a MPLS backbone that will stem from the continents main Data center, in our case this is Johannesburg.

For all other internal connectivity we have been assigned the protocol IS-IS for intenral routing.

## Requirements 

As provided in the customers requirement document each team has been assigned a continent in the aims of providing reliable and fast interconnectivity for that specified contient, as such the Afriacn region has been designated to provide its connectivity via the ISIS protocol for its IGP.

Additionally the customer has set out a series of expectations that need to be met these are the following.

    * IGP will be IS-IS
    * The AS Number will be 65005
    * IPV4 Connectivity at the server sites.
    * IPv6 Connectivity at all other sites.
    * DHCPv6 and/or EUI-64
    * EUI-64 or /127 Links to directly connected peers.
    * IPv6 or IPv4 Tunnels on the MPLS backbone
  
### Resources

___

This section pertains to the resources assigned to the consultant team as set out in the requirements document.

#### Physical Resources

This section pertains to all phyiscal devices and assigned as requirements throughout the project.

| Brand | Type | Purpose | Specifications link |
|---|---|---|---|
| Intel | Blade Server H2312JFJR | ESXI + VM Hosting | [Intel Server System H2312JFJR](https://ark.intel.com/products/56309/Intel-Server-System-H2312JFJR) |
| Juniper | SRX240 Router | P/PE Routers for MPLS | [Juniper SRX240](https://www.juniper.net/documentation/en_US/release-independent/junos/topics/reference/specifications/chassis-srx240-description.html) |

#### Virtual Resources

This section pertains to the software and licenses that will be required throughtout the project.

| Name | Software Type |License Type | Purpose |
|---|---|---|---|
| ESXI 6.5 | Hypervisor | Student | To be installed to manage Virtual machines on the blade |
| vMX | Junos OS | Student | Router |
| vSRX | Junos OS | Student | Security Router |
  
## Team Members

This section sets out each member of the group and their relevent contact information, addiotionally dates have been set for when the role of Project Manager is rotated on a fortnitely basis.

|Name|Email| PM Period 1 |
|---|---|---|
| Bo Mikkelsen | boxx1483@edu.eal.dk | 20/08/18 - 02/09/18 |
| Kyle Mcshane | kyle0055@edu.eal.dk | 03/09/18 - 16/09/18 |
| Kasper Jessen | kasp6851@edu.eal.dk | 17/09/18 - 30/09/18 |
| Tim Lyder | timx1571@edu.eal.dk | 01/10/18 - 14/10/18  |
| Martin Neilson | mart074a@edu.eal.dk | 22/10/18 - 05/11/18 |