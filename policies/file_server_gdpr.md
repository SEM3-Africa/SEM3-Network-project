# File-Server GDPR Access Policy

    Author              : Kyle Mcshane
    Purpose             : How to handle GDPR compliancy with the file-server.
    Date of Creation    : 06-11-2018

## Version Control

| Revision | Date       | Comments                                                                                             | Changes By   |
| -------- | ---------- | ---------------------------------------------------------------------------------------------------- | ------------ |
| 1.0      | 06-10-2018 | Guidlines how to handle data that is stored on the fileserver in regards to reaching GDPR compliancy | Kyle Mcshane |

### File Server

Users will be able to access the network via the use of a configured public VLAN that will grant basic access to the network and the fileservers - as such data that has been uploaded must be subject to a policy that covers the following aspects.

    Consent must be crystal-clear.

Login-Page with sign up should be able to cover this.

    You must collect the minimum amount of information needed to achieve your objective.
Self Explanitory.

    Consumers have the right to access and review the data you collect from them.
As the fileserver is the only data we will be hosting we should easily be able to handle this as it's readibly avaidable.

    Consumers have the right to have the data you collect be “forgotten”
This should be easily coverable by allowing users to delete their uploaded data and make sure any backups reflect this too.