# Physical Access Policy

    Author              : Kyle Mcshane
    Purpose             : To layout how physical access to the servers and project should be handled.
    Date of Creation    : 05-11-2018

## Version Control

| Revision | Date       | Comments                                                                | Changes By   |
| -------- | ---------- | ----------------------------------------------------------------------- | ------------ |
| 1.0      | 05-10-2018 | Created initial document to layout how to handle physical server access | Kyle Mcshane |

## Physical Access

Physical access to the project servers should only ever be a last resort as all configurations and servers are set up and in place - as such in the event that physicall access is required as a last resort it should be handled in the following format.

### UCL Security Policy

The location for the servers and sites are located at the place of education most notably UCL Odense and as such we have to adhere and rely to their security policies and practises, as such we have to operate on the following information

#### Building Access

| Dates   | Time          | Notes                                                                                                                      |
| ------- | ------------- | -------------------------------------------------------------------------------------------------------------------------- |
| Mon-Fri | 07:00 - 16:30 | from these hours the building is classed as open and general access is permitted.                                          |
| Mon-Fri | 16:31 - 06:59 | Personal keyfob & code is required to enter the building and is logged, doors and window alarms are armed during this time |
| Mon-Fri | 22:30 - 07:00 | Central Alarm is activated                                                                                                 |
| Sat-Sun | All day       | Access is only permitted through main doors and all alarms are activated on outerdoors & windows                           |

### Server Access

In order to access the server room you must first have to gain entrance to the school in the method described above, Physical access to the project should only ever be handled as a last resort as all configuration and set up should be done via a SSH connection with monitored & logged login sessions - however in the event that physical access is required and all other methods have been exhausted the server room will be accessable via certain keyfobs only obtained by certain class members.