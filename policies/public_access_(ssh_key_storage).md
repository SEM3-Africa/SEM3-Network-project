# Public SSH Key Storage

    Author              : Tim Nikolajsen
    Purpose             : To define how public SSH keys are stored and how to restore access to the system.
    Date of Creation    : 07-11-2018

## Version Control

| Revision | Date       | Comments                                                                | Changes By     |
| -------- | ---------- | ----------------------------------------------------------------------- | -------------- |
| 1.0      | 07-10-2018 | Created initial document												  | Tim Nikolajsen |

## Storage of public SSH Keys

Public SSH keys are stored on Gitab.com where the project is managed.

### How to lose access

Access to the project/system is lost when the user loses his/hers private SSH key used to create the public key.

#### How to regain access

To regain access to the project/system the user without access must gain access from a project/system member with priveleges to handle SSH keys.

##### Procedure

- Add new public SSH key to your Gitlab.com user.
- Replace you ssh key in the config files in the config files [project](https://gitlab.com/SEM3-Africa/SEM3-Config-FIles) on each device.