# Report Folder

This folder contains files necessary for the final report for 3rd Semester - Network.

## Complete_Documents

Contains the finished documents in PDF formation ready for hand-in.

## Separate_Documents

Contains each separate document ready for consolidation into the complete document.