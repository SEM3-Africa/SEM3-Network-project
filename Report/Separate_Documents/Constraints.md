# Contraints

During the project multiple limitations was discovered which had to be overcome to create a working network with them being:

## ESXI Limitations - Link Aggregation

Due to the way VMWare is doing their virtual network adapters there is a limitation of 10 adapters per virtual device in ESXi environment.
This prevented the implementation of the link aggregation configuration that was planned in the virtual environment. The planned link aggregation would put the amount of adapters above 10. In the physical environment this limitation does not exist and will therefore work without any problems. This resulted in link aggregation not being used in the virtual environment.

## VPN

The VPN is not setup in the virtual environment as the school already have a VPN running and it does not make sense to VPN twice to connect directly to the system. What was done instead is connect the project to the schools network which allows us to connect to the network as if we VPN'ed directly.

## DNSMASQ - DHCPv6

DNSMASQ has a problem with DHCPv6 where it cannot be set up to work correctly. This was circumvented by using ISC DHCP instead.

## MPLS Backbone - Single Point of Failure

The MPLS backbone contains a single point of failure between each site and their PE router. This is due to a limitation in amount of hardware available for the project. But in a physical network this would not be done in this way as there would be at least two internet connections which each would connect to two PE routers to improve resilience.