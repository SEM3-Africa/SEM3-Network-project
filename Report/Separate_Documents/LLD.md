# Low Level Design

The low level design details the configuration of the different devices or networks so anyone can configure the device correctly from the low level design alone.

## Spreadsheets

This spreadsheet shows the configuration of the physical network that is meant to be implemented.

[Physical](https://gitlab.com/SEM3-Africa/SEM3-Network-project/raw/master/docs/LLD/LLD_-_Physical.png)

This spreadsheet shows the configuration of the virtual network which is the result of programming limitations in the ESXi.

[Virtual](https://gitlab.com/SEM3-Africa/SEM3-Network-project/raw/master/docs/LLD/LLD_-_Virtual.png)

This spreadsheet shows the configuration of the management router.

[Management](https://gitlab.com/SEM3-Africa/SEM3-Network-project/raw/master/docs/LLD/LLD_-_MGMT.png)

Below is the configuration of the Firewall between Africa and the MPLS backbone. Each link shows a different part of the configuration.

[Security Zones](https://gitlab.com/SEM3-Africa/SEM3-Network-project/raw/master/docs/LLD/LLD_AfricaFW_-_Security_Zones.png)

[Security Policies](https://gitlab.com/SEM3-Africa/SEM3-Network-project/raw/master/docs/LLD/LLD_AfricaFW_-_Security_Policies.png)

[Security Screens](https://gitlab.com/SEM3-Africa/SEM3-Network-project/raw/master/docs/LLD/LLD_AfricaFW_-_Security_Screens.png)

[Address-Book](https://gitlab.com/SEM3-Africa/SEM3-Network-project/raw/master/docs/LLD/LLD_AfricaFW_-_Security_Address-Book.png)

# Low Level Design

The low level design details the configuration of the different devices or networks so anyone can configure the device correctly from the low level design alone.

| Network / Device																												    | Description																						   							|
| --------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------- |
| [Physical](https://gitlab.com/SEM3-Africa/SEM3-Network-project/raw/master/docs/LLD/LLD_-_Physical.png) 							| This spreadsheet shows the configuration of the physical network that is meant to be implemented. 							|
| [Virtual](https://gitlab.com/SEM3-Africa/SEM3-Network-project/raw/master/docs/LLD/LLD_-_Virtual.png) 	 							| This spreadsheet shows the configuration of the virtual network which is the result of programming limitations in the ESXi. 	|
| [Management](https://gitlab.com/SEM3-Africa/SEM3-Network-project/raw/master/docs/LLD/LLD_-_MGMT.png)   							| This spreadsheet shows the configuration of the management router. 															|
| [Security Zones](https://gitlab.com/SEM3-Africa/SEM3-Network-project/raw/master/docs/LLD/LLD_AfricaFW_-_Security_Zones.png) 		| This spreadsheet shows the configuration of the security zones on the firewall between Africa and the MPLS backbone.		 	|
| [Security Policies](https://gitlab.com/SEM3-Africa/SEM3-Network-project/raw/master/docs/LLD/LLD_AfricaFW_-_Security_Policies.png) | This spreadsheet shows the configuration of the security policies on the firewall between Africa and the MPLS backbone. 		|
| [Security Screens](https://gitlab.com/SEM3-Africa/SEM3-Network-project/raw/master/docs/LLD/LLD_AfricaFW_-_Security_Screens.png) 	| This spreadsheet shows the configuration of the security screens on the firewall between Africa and the MPLS backbone. 		|
| [Address-Book](https://gitlab.com/SEM3-Africa/SEM3-Network-project/raw/master/docs/LLD/LLD_AfricaFW_-_Security_Address-Book.png) 	| This spreadsheet shows the configuration of the global address.book on the firewall between Africa and the MPLS backbone. 	|