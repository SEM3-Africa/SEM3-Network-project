# Week 36

This directory will detail the tasks and work undertook during the working period of 27/08/2018 - 02/08/2018.

## Format

    * ❌ - Missed Deadline / Pushed to Next Week
    * # - On going / Span Multiple Weeks.
    * ✔ - Completed

| Task                                            | Asignee        | Status | Expected Completion Date | Additional Comments                                                 |
| ----------------------------------------------- | -------------- | ------ | ------------------------ | ------------------------------------------------------------------- |
| Add further detail to the repository readme     | Martin Nielsen | #      | #                        | Keeping this live - considering a table of contents                 |
| Networking hardware list with OPEX / APEX costs | Kyle Mcshane   | ✔      | 04/09/18                 | Was moved into buissness costs document to calculate project costs. |
| Project Restructuring                           | Bo Mikkelsen   | ✔      | 03/09/18                 | Project Gitlab was restructured into groups                         |
| Completion & Review of Business Case            | Bo Mikkelsen, Kasper Jessen   | ✔      | 05/09/18           | Draft was reviewed and ready to submit to client                    |
| Debugging 101 ESXi vs Boys | Bo, Martin, Tim, Kasper | ✔ | 05/09/18 | Fixed some troubles with the ESXi |