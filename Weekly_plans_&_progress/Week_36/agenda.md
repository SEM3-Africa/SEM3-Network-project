# ESXI SS1 Week 36 Agenda

    Date : 07/09/2018
    Time & Durination : 11:15-11:30
    Mentor Attendee(s) : Morten Nielsen
    Team Attendee(s) : Kyle Mcshane

The discussion will be aimed at exploring current technical issues and ideas that the group has gone through/presented
additionally current progress with the ESXI will be discussed aswell as any other buisness relating to the project.

## Progress on the ESXI

* Initial Errors of Knowledge relating to the ESXI.
* Docker - LibreNM, Webserver (Automatization)

## Things to consider moving forward

* Discuss current progress with the ESXI
* Current plans in motion - plans for progress
* Presentation Review - What could the team do better / format.
* Types of documentation we should be producing / thereof lack of.
* Participation & Communication issues

## Technical Plans for the ESXI

    07/09/2018 and forward.
* Switching over to a Static IP to avoid DHCP lease Issues.
* Docker 
* Move the Jumphost / Web Client to a different physical interface.
* Port Fowarding
* Documentation - steps so far
* Decide on a SSH Proxy through an SRX or use a proxy Jump Host

Also up for discussion will be any topic that will be relevant - pertains to the current state of the ESXI / Networking project.


  