# Week 35

This directory will detail the tasks and work undertook during the working period of 27/08/2018 - 02/08/2018.

## Format

    * ❌ - Missed Deadline / Pushed to Next Week
    * # - On going / Span Multiple Weeks.
    * ✔ - Completed

| Task                                           | Asignee        | Status | Expected Completion Date | Additional Comments                                                                                                         |
| ---------------------------------------------- | -------------- | ------ | ------------------------ | --------------------------------------------------------------------------------------------------------------------------- |  |
| Add further detail to the repository readme    | Kyle Mcshane   | #      | #                        | Keeping this live - considering a table of contents                                                                         |
| Added in a inventory of devices                | Kyle Mcshane   | #      | #                        | [Initial draft needs to be kept live.](https://gitlab.com/syondi/SEM3-NETPROJ/tree/master/docs/Inventory%20of%20Devices)    |
| Copied and uploaded team contract signatures   | Kyle Mcshane   | ✔      | 21/08/2018               | [Signature Page](https://gitlab.com/syondi/SEM3-NETPROJ/blob/master/docs/Work_agreements/Team-contract.png)                 |
| Trying to create a business strategy           | Kasper Jessen  | #      | #                        | [Needs some work](https://gitlab.com/syondi/SEM3-NETPROJ/blob/master/docs/Business/business_strategy/Business_strategy_.md) |
| Writing on Business Continuity Plan            | Martin Nielson | #      | #                        | [Running issues](https://gitlab.com/syondi/SEM3-NETPROJ/blob/master/docs/Business_Continuity_Plan/BCP.md)                   |
| Added IPs for Each MX and the SRX into the LLD | Kyle Mcshane   | #      | #                        | Would like some feedback on this.                                                                                           |
| Hardware requirements                          | Tim Nikolajsen | ✔      | 30/8/2018                | This was added to the business case.                                                                                        |
| Spreadsheet for equipment cost                 | Kasper Jessen  | ✔      | 30/8/2018                |                                                                                                                             |