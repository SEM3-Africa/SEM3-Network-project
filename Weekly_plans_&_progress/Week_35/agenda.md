# Week 35 Meeting Agenda

## Group 5  - ESXI subject
<!-- pagebreak -->

####Tables of contents
- [Week 35 Meeting Agenda](#week-35-meeting-agenda)
    - [Group 5 - ESXI subject](#group-5---esxi-subject)
        - [1. **Discuss the progress on the EXSI SS**](#1-discuss-the-progress-on-the-exsi-ss)
        - [2. **What should we do, going forward**](#2-what-should-we-do-going-forward)
        - [3. **Plan with the ESXI right now(Technical)**](#3-plan-with-the-esxi-right-nowtechnical)

<!-- pagebreak -->

### 1. **Discuss the progress on the EXSI SS**
    - How far are we?
    - How have we manged the project?
    - Should we handle documentation differently?

<!-- pagebreak -->

### 2. **What should we do, going forward**
    - What have we learned so far, that should be improved?
    - How should we improve it?
    - Weekly Presentation should be more prepared?

<!-- pagebreak -->

###  3. **Plan with the ESXI right now(Technical)**
    - What are we going to do on the ESXI next week(36)?
    - Static Addres? how do we handle the management of the ESXi?
