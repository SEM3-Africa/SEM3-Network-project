# Intial Group Meeting week 39 | Project going forward

___
## 23/09/2018  || 8:15

___
**Someone take minutes of meeting**

**1. Current state of project**
* Problems
  * No goals, no overview of each weekly tasks, issue tracking.
* No Individual tasks for each week
  * Work on tasks before implementing non-goal oriented
* Need a non MPP Project_plan
  * Include goals etc.
* **Missing documentaion**
* Random implementations before main tasks
___

**2. How to get back on the right track**
* Difine Overall goal and sub goals
* Write project plan (no mpp)
  * Why do we do this? etc.
* Write full MPP for all weeks, that needs to be atleast done.
* Issue tracking, connected with MPP
  * Issues commenting, and active closing
  * Schedule
* Overall milestones to fit goals
* Documentation of features (Why we use / How we use)
* Implementation of main tasks before off-topic ones.
* Communication
* **One active PM**
___

**3. Other dicussions**

* Stakeholders?
  * Getting someone involved from the outside?
* **Present goals, project changes to Morten for feedback**
* Other topics?