# Week 38 Meeting Notes

## Agenda

This file will pertain to the group meeting we held between all group membmers on the 24/09/2018 the goal of this meeting was to restructure the entire project and provide a more clear & defined set of goals and tasks for us to work from the future.

    Members: Kasper, Tim, Kyle, Bo, Martin
    Date:   24/09/2018
    Time:   8:15 AM

## Discussed Points

* Poor issues tracking and project progress
* The need for more strucuture and goals/milestones/tasks
* Each memeber needs an individual each week so they have something clear to work on - as a result of this there is a lot of off topic work where we are derailing from the project and working on irrelevant tasks
* We need a more structured project plan seperate from peters for each SS 
* There is missing documentation on configurations AND reasoning why we use certain programs and services - a fix or this would be a defined overall goal and sub-goal and make sure they are clear and defined and have documentation on each of them BEFORE we start working on them.
* A project plan with the defined goals - similar to the buisness case.
* Issue clean-up and tracking and more defined milestones as the ones that are currently set are way to vauge and dont provide any clear context on where we will arive to.
* More participation in the issues and tracking meaning that we all need to contribute more in the issues in the form of commenting
* Issues should be more detailed and involve alot of information pertaining to what they issue will require and achive rather than being a vauge point in the future.
* Current running documentation needs to be defined on current running machines and services and why we have them.
* A single assigned PM - where we can all come to a common strucutre and adhere too.
* Having a single PM will not dump all the admin work on the PM - this is a group project and the only duties the PM will take is the final vote on issues/milestones and be responsible for meetings with the teachers / presentations.
* We aim to keep the current Project network seperate from the special subject and set clear defined working hours between them - as having PN run all week will add some confusion to the time scale we actually have with SS.

## Defined Sub Goals

In the meeting we dicussed redefining what sub-goals meant to us and what we would do to work towards them, we came to the consensus that...

* Should be off-topic points that do not cover the requirements - meaning things that would be "nice" to have rather than neccecary to have.
* Main goals should take presedence over these subgoals and before we hit these sub-goals we must work to reach the requirements first.
* A goal like this might be something like make a fully automated back up system - something that is relative and has a practical use but at the same time is not part of the final requirements.