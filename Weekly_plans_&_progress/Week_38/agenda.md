# ESXI SS1 Week 38 Agenda

    Date : 20/09/2018
    Durination : 15 Minutes
    Mentor Attendee(s) : Morten Nielsen
    Team Attendee(s) : Kasper Jessen

The discussion will be aimed at exploring current technical issues and ideas that the group has gone through/presented. Additionally current progress with the ESXI will be discussed aswell as any other buisness relating to the project.

## Progress on the ESXI

* ESXi limitations/problems
* Docker (Webserver, VPN, MariaDB, LibreNMS)

## Things to consider moving forward

* Remember documentation - Better documentation
* Dont exceed requirements just to create additional problems
* Communication issues

## Technical Plans for the ESXI

* Additional ELK stack for monitoring?
* Swapping from test environment to live blade
