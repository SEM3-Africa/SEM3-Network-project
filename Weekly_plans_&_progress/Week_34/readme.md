# Week 34

This directory will detail the tasks and work undertook during the working period of 20/08/2018 - 26/08/2018.

## Format

    * ❌ - Missed Deadline / Pushed to Next Week
    * # - On going / Span Multiple Weeks.
    * ✔ - Completed

|Task|Asignee|Status|Expected Completion Date|Additional Comments|
|---|---|---|---|---|---|
|Draft LLD| Kyle Mcshane | ✔  | 24/08/18 | Created a draft but is missing detail |
|Team weekly detail| Kyle Mcshane | ✔ | 24/08/18 | This document needs revision
|Landing page readme| Kyle Mcshane |✔ | 24/08/18 | Still requires more detail
|Installing ESXi| Kasper Jessen |✔ | 24/08/18 | Still requires more detail
|Draft HLD| TIm Nikolajsen |✔ | 24/08/18 | Still requires more detail
|Create and mange Project plan till week 38| Bo Mikkelsen|✔|22/08/18| Running changes thorugh the whole project|
|Finish business case for project|Bo Mikkelsen|✔ |26/08/18|Documented outside workhours, exceeded time|
|Make Code Of Coduct for the group|Martin Nielsen|✔ |23/08/18| Feel free to add some points to it.|
|||||